import org.joml.Vector2i;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Queue;

public class BFS<T> extends Search<BFS<T>, T> {

  private Grid.Predicate<T> isGoal;
  private Grid.Function<T> replaceGoalWith = (x, y, t) -> t;

  @Override protected BFS<T> getThis() { return this; }

  public BFS() {
    this.start = new Vector2i(0, 0);
    returnAllGoals();
    visitNonEmpty();
    findEverything();
  }

  public BFS(int x, int y) {
    this.start = new Vector2i(x, y);
    returnAllGoals();
    visitNonEmpty();
    findEverything();
  }

  public BFS(BFS<T> bfs) {
    this.start = bfs.start;
    this.min = bfs.min;
    this.max = bfs.max;
    this.branchDirections = bfs.branchDirections;
    this.maxDistance = bfs.maxDistance;
    this.shouldVisit = bfs.shouldVisit;
    this.isGoal = bfs.isGoal;
    this.replaceGoalWith = bfs.replaceGoalWith;
    this.maxNumGoals = bfs.maxNumGoals;
  }

  public BFS isGoalIf(Grid.Predicate<T> isGoal) {
    this.isGoal = isGoal;
    return getThis();
  }

  public BFS findEverything() {
    return isGoalIf((x, y, t) -> t != null && t != this.defaultValue);
  }


  public BFS forEachGoalReplaceWith(Grid.Function<T> replaceGoalWith) {
    this.replaceGoalWith = replaceGoalWith;
    return getThis();
  }

  public BFS noSideEffects() {
    return forEachGoalReplaceWith((x, y, t) -> t);
  }

  public BFS doNothing() {
    return noSideEffects();
  }

  public BFS forEachGoalDoNothing() {
    return noSideEffects();
  }

  public BFS<T> copy() {
    return new BFS<T>(this);
  }

  /** @return true if the search finds something */
  public boolean someSuchNode(Grid<T> grid) {
    /* only one goal needs to be found, then short-circuit and return true */
    int prevMaxGoals = this.maxNumGoals;
    returnFirstGoal();

    /* disallow side-effects for simple predicate search */
    Grid.Function<T> prevFunc = this.replaceGoalWith;
    noSideEffects();

    boolean someSuchNode = !search(grid).isEmpty();

    /* restore state */
    returnAtMost(prevMaxGoals);
    forEachGoalReplaceWith(prevFunc);

    return someSuchNode;
  }

  public boolean noSuchNode(Grid<T> grid) {
    return !someSuchNode(grid);
  }

  @Override
  protected void run(Grid<T> grid, Collection<Vector2i> fill,
  Grid<Boolean> visited, Grid.Predicate<T> visitNeighborIf) {
    Queue<Vector2i> frontier = new LinkedList<Vector2i>();
    frontier.add(start);

    // perform search
    while(!frontier.isEmpty() && (fill.size() < maxNumGoals || maxNumGoals < 1)) {
      Vector2i current = frontier.poll();
      visited.put(current.x, current.y, true);

      if(isGoal.test(current, grid.get(current))) fill.add(current);

      grid.getCoords(current.x, current.y, branchDirections, frontier, visitNeighborIf);
    }

    // perform mapping function over found goals
    for(Vector2i v : fill) {
      T replace = replaceGoalWith.apply(v.x, v.y, grid.get(v.x, v.y));
      grid.put(v, replace);
    }
  }

  public static <T> BFS<T> findAllContiguous() {
    return new BFS<T>().visitNonEmpty().findEverything();
  }

  public static <T> BFS<T> findContiguous(Grid.Predicate<T> isGoal) {
    return new BFS<T>().visitNonEmpty().isGoalIf(isGoal);
  }

  public static <T> BFS<T> forEachContiguous(Grid.Function<T> forEach) {
    return BFS.<T>findAllContiguous().forEachGoalReplaceWith(forEach);
  }

  public static <T> BFS<T> searchEverything() {
    return new BFS<T>().visitEverything().findEverything();
  }

  public static <T> BFS<T> visitEverythingWithinDistance(int distance) {
    return new BFS<T>().visitEverything().searchWithinDistance(distance);
  }
}
