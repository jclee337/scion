import org.joml.Vector2i;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
* represents a 2D grid of arbitrary size as a map
*/
public class Grid<T> {

  private HashMap<Integer, T> grid = new HashMap<>(); // backing map
  private T def; // default value returned when no such key exists

  /*
   * Min and max coords are remembered so BFS has something to fall back on
   * and doesn't get caught in an infinite loop.
   */
  private Vector2i min = new Vector2i(0, 0);
  private Vector2i max = new Vector2i(0, 0);
  public static final List<Vector2i> ORTHO = Arrays.asList(
    new Vector2i(-1,  0),
    new Vector2i( 0, -1),
    new Vector2i( 1,  0),
    new Vector2i( 0,  1));
  public static final List<Vector2i> OMNI = Arrays.asList(
    new Vector2i(-1,  0),
    new Vector2i( 0, -1),
    new Vector2i( 1,  0),
    new Vector2i( 0,  1),
    new Vector2i(-1, -1),
    new Vector2i( 1, -1),
    new Vector2i( 1,  1),
    new Vector2i(-1,  1));

  public Grid() {
    this.def = null; /* the default default (lol) is null */
  }

  public Grid(T def) {
    this.def = def;
  }

  public Grid(Grid<T> other) {
    this.grid = new HashMap<Integer, T>(other.grid);
    this.def = other.def;
  }

  public T getDefault() {
    return this.def;
  }

  public T get(int x, int y) {
    return grid.containsKey(hash(x, y)) ? grid.get(hash(x, y)) : def;
  }

  public T get(Vector2i v) {
    return get(v.x, v.y);
  }

  public boolean has(int x, int y) {
    return grid.containsKey(hash(x, y));
  }

  public boolean has(Vector2i v) {
    return has(v.x, v.y);
  }

  public boolean isEmpty(int x, int y) {
    return !has(x, y);
  }

  public boolean isEmpty(Vector2i v) {
    return isEmpty(v.x, v.y);
  }

  /**
   * Inserts the value into the grid at the given coordinates.
   * If the given value is the default value, the previous value is removed.
   */
  public void put(int x, int y, T val) {
    if(val == null || val.equals(def)) {
      remove(x, y);
    } else {
      min.x = Math.min(x, min.x);
      min.y = Math.min(y, min.y);
      max.x = Math.max(x, max.x);
      max.y = Math.max(y, max.y);
      grid.put(hash(x, y), val);
    }
  }

  public void put(Vector2i v, T val) {
    put(v.x, v.y, val);
  }

  /**
   * Tries to remove the item at the given coordinates.
   * @return the removed value or default value if coordinate is empty
   */
  public T remove(int x, int y) {
    return grid.containsKey(hash(x, y)) ? grid.remove(hash(x, y)) : def;
  }

  public T remove(Vector2i v) {
    return remove(v.x, v.y);
  }

  public void move(int origx, int origy, int destx, int desty) {
    put(destx, desty, get(origx, origy));
    remove(origx, origy);
  }

  public void move(Vector2i origin, Vector2i destination) {
    move(origin.x, origin.y, destination.x, destination.y);
  }

  /**
   * @return the number of non-default items inserted into the grid map
   */
  public int count() {
    return grid.size();
  }

  /**
   * The distance according to the way a queen moves on a chessboard.
   * @return the chebyshev distance between (x1, y1) and (x2, y2)
   */
  public static int chebyshev(int x1, int y1, int x2, int y2) {
    return Math.max(Math.abs(x2 - x1), Math.abs(y2 - y1));
  }

  /**
   * The distace according to the way a rook moves on a chessboard.
   * @return the manhattan distance between (x1, y1) and (x2, y2)
   */
  public static int manhattan(int x1, int y1, int x2, int y2) {
    return Math.abs(x2 - x1) + Math.abs(y2 - y1);
  }

  public static boolean withinArea(int x, int y, int minx, int miny, int maxx, int maxy) {
    return x >= minx && y >= miny && x <= maxx && y <= maxy;
  }

  public boolean withinBounds(int x, int y) {
    return withinArea(x, y, this.min.x, this.min.y, this.max.x, this.max.y);
  }

  public Collection<T> get(Collection<T> fill, Collection<Vector2i> coords) {
    for(Vector2i v : coords) {
      fill.add(get(v.x, v.y));
    }
    return fill;
  }

  /**
   * @param x x-coordinate of value to check
   * @param y y-coordinate of value to check
   */
  public boolean test(int x, int y, Predicate<T> pred) {
    return (!isEmpty(x, y) || def != null) && pred.test(x, y, get(x, y));
  }

  public void forEach(int x, int y, Collection<Vector2i> each,
  Predicate<T> pred, Consumer<T> cons) {
    for(Vector2i v : each) {
      int nx = x + v.x, ny = y + v.y;
      if(test(nx, ny, pred)) cons.accept(nx, ny, get(nx, ny));
    }
  }

  public void forEach(Vector2i v, Collection<Vector2i> each,
  Predicate<T> pred, Consumer<T> cons) {
    forEach(v.x, v.y, each, pred, cons);
  }

  /**
  * @return true if at least one neighbor of (x, y) satisfies the predicate
  */
  public boolean some(int x, int y, Collection<Vector2i> each, Predicate<T> pred) {
    for(Vector2i v : each) {
      if(test(x + v.x, y + v.y, pred)) return true;
    }
    return false;
  }

  /**
  * @return true if all neighbors of (x, y) satisfy the predicate
  */
  public boolean all(int x, int y, Collection<Vector2i> each, Predicate<T> pred) {
    return !some(x, y, each, pred.negate());
  }

  public <C extends Collection<T>> C get(int x, int y,
  Collection<Vector2i> each, C fill, Predicate<T> pred) {
    forEach(x, y, each, pred, (int xx, int yy, T t) -> { fill.add(t); });
    return fill;
  }

  public LinkedList<T> get(int x, int y, Collection<Vector2i> each, Predicate<T> pred) {
    return get(x, y, each, new LinkedList<T>(), pred);
  }

  public <C extends Collection<T>> C get(Vector2i v,
  Collection<Vector2i> each, C fill, Predicate<T> pred) {
    return get(v.x, v.y, each, fill, pred);
  }

  public LinkedList<T> get(Vector2i v, Collection<Vector2i> each, Predicate<T> pred) {
    return get(v, each, new LinkedList<T>(), pred);
  }

  public Collection<Vector2i> getCoords(int x, int y,
  Collection<Vector2i> each, Collection<Vector2i> fill, Predicate<T> pred) {
    forEach(x, y, each, pred, (xx, yy, t) -> { fill.add(new Vector2i(xx, yy)); });
    return fill;
  }

  public Collection<Vector2i> getCoords(int x, int y,
  Collection<Vector2i> each, Predicate<T> pred) {
    return getCoords(x, y, each, new LinkedList<Vector2i>(), pred);
  }

  /**
  * @return unique integer ID from a coordinate pair
  */
  public int hash(int x, int y) {
    return (x << 16) + y;
  }

  public Collection<Vector2i> search(BFS<T> bfs) {
    return bfs.search(this);
  }

  @FunctionalInterface
  public interface Predicate<T> {
    public boolean test(int x, int y, T t);

    default boolean test(Vector2i v, T t) {
      return test(v.x, v.y, t);
    }

    default Predicate<T> negate() {
      return (x, y, t) -> { return !this.test(x, y, t); };
    }

    default Predicate<T> and(Predicate<? super T> other) {
      return (x, y, t) -> { return this.test(x, y, t) && other.test(x, y, t); };
    }

    default Predicate<T> or(Predicate<? super T> other) {
      return (x, y, t) -> { return this.test(x, y, t) || other.test(x, y, t); };
    }
  }

  @FunctionalInterface
  public interface Function<T> {
    public T apply(int x, int y, T t);

    default Function<T> then(Function<T> other) {
      return (x, y, t) -> { return other.apply(x, y, this.apply(x, y, t)); };
    }
  }

  @FunctionalInterface
  public interface Consumer<T> {
    public void accept(int x, int y, T t);
  }
}
