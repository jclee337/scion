
public abstract class CombatStrategy {

  /**
   * @return true when the strategy is done doing its combat routine
   */
  public abstract boolean step(ShardGrid attacked, Stats attacker, Grid<Boolean> origins, Grid<Boolean> selected);
}
