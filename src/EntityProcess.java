
@FunctionalInterface
public interface EntityProcess {
  public void process(int entity);
}
