import com.artemis.World;

import java.util.HashMap;
import java.util.Map;

/**
 * This class is for storing input callbacks.
 * Those are actions to do when a key or mouse button is pressed.
 */
public class Bindings {
  private World world;
  private int entity;
  private boolean enabled = false;
  private Map<Integer, Thunk> bindings = new HashMap<>();
  public Mode mode = Mode.CONSUMING;
  public Thunk beforeInputDo = Thunk.NOOP;
  public Thunk afterInputDo = Thunk.NOOP;
  public int mousex = -1, mousey = -1;

  public Bindings(World world) {
    this.world = world;
    this.entity = world.create();
  }

  public Bindings put(int key, int mods, int action, Thunk t) {
    bindings.put(hash(key, mods, action), t);
    System.out.println(hash(key, mods, action));
    return this;
  }

  public Thunk get(int key, int mods, int action) {
    return bindings.get(hash(key, mods, action));
  }

  public boolean has(int key, int mods, int action) {
    return bindings.containsKey(hash(key, mods, action));
  }

  public void enable() {
    Context c = world.getMapper(Context.class).create(entity);
    c.bindings = this;
    enabled = true;
  }

  public void disable() {
    world.getMapper(Context.class).remove(entity);
    enabled = false;
  }

  public boolean enabled() {
    return enabled;
  }

  /**
   * | upper bits | middle 4 bits | lowest 2 bits |
   * |  keycode   |      mods     |     action    |
   * @return a unique identifier for combination of key, mods, & action
   */
  private int hash(int key, int mods, int action) {
    return (((key << 4) | mods) << 2) | action;
  }

  public enum Mode {
    /* This context consumes all input, if on top */
    CONSUMING,

    /* This context defers input to lower contexts, if it doesn't have a particular binding */
    DEFERRING,

    /* This context always receives all input */
    UNIVERSAL
  }
}
