import com.artemis.*;
import com.artemis.utils.IntBag;
import org.lwjgl.BufferUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import static org.lwjgl.BufferUtils.*;

public class Utils {

  public static EntitySubscription.SubscriptionListener listener(
    World world, Aspect.Builder aspect, EntityProcess insert, EntityProcess remove) {
    EntitySubscription subscription = world.getAspectSubscriptionManager().get(aspect);
    EntitySubscription.SubscriptionListener s = new EntitySubscription.SubscriptionListener() {
      @Override
      public void inserted(IntBag entities) {
        int[] ids = entities.getData();
        for(int i = 0; i < entities.size(); i++) {
          insert.process(ids[i]);
        }
      }

      @Override
      public void removed(IntBag entities) {
        int[] ids = entities.getData();
        for(int i = 0; i < entities.size(); i++) {
          remove.process(ids[i]);
        }
      }
    };
    subscription.addSubscriptionListener(s);
    return s;
  }

  public static Integer max(Integer... vals) {
    Integer res = null;
    for(Integer val : vals) {
      if(res == null || (val != null && val > res)) {
        res = val;
      }
    }
    return res;
  }

  public static String loadResource(String fileName) throws Exception {
    String result = "";
    try (InputStream in = Utils.class.getClass().getResourceAsStream(fileName)) {
      result = new Scanner(in, "UTF-8").useDelimiter("\\A").next();
    }
    return result;
  }

  public static ByteBuffer ioResourceToByteBuffer(String resource, int bufferSize) throws IOException {
    ByteBuffer buffer;

    Path path = Paths.get(resource);
    if ( Files.isReadable(path) ) {
      try (SeekableByteChannel fc = Files.newByteChannel(path)) {
        buffer = BufferUtils.createByteBuffer((int)fc.size() + 1);
        while ( fc.read(buffer) != -1 ) ;
      }
    } else {
      try (
      InputStream source = Thread.currentThread().getContextClassLoader().getResourceAsStream(resource);
      ReadableByteChannel rbc = Channels.newChannel(source)
      ) {
        buffer = createByteBuffer(bufferSize);

        while ( true ) {
          int bytes = rbc.read(buffer);
          if ( bytes == -1 )
          break;
          if ( buffer.remaining() == 0 )
          buffer = resizeBuffer(buffer, buffer.capacity() * 2);
        }
      }
    }

    buffer.flip();
    return buffer;
  }

  private static ByteBuffer resizeBuffer(ByteBuffer buffer, int newCapacity) {
    ByteBuffer newBuffer = BufferUtils.createByteBuffer(newCapacity);
    buffer.flip();
    newBuffer.put(buffer);
    return newBuffer;
  }
}
