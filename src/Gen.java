import com.artemis.*;
import com.artemis.utils.IntBag;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

import static org.lwjgl.opengl.GL11.*;

/**
 * This is a static class of methods for generating various game objects.
 * The function names should be self-explanatory as to what they generate.
 */
public class Gen {
  private static Random random = new Random();
  private static int layer = 0;

  public static ShardGrid genGrid(ShardGrid grid, int lower, int upper) {
    List<Shard> unplaced = new ArrayList<Shard>();
    int area = grid.width() * grid.height();
    for(; area > 6 ;) {
      Shard shard = genShard(lower, upper);
      unplaced.add(shard);
      area -= shard.nodes.size();
    }
    layer = 0;
    System.out.println("attempting to place " + unplaced.size() + " # of shards");
    return genGrid(grid, unplaced);
  }

  private static ShardGrid genGrid(ShardGrid grid, List<Shard> shards) {
    if(shards.isEmpty()) {       /* all shards have been placed successfully */
      return grid;
    }
    Shard next = shards.remove(0);
    for(int y = 0; y < grid.height(); y++) {
      for(int x = 0; x < grid.width(); x++) {
        if(grid.canPlace(next, x, y)) {
          layer++;
          grid.placeShard(next, x, y);
          ShardGrid attempt = genGrid(grid, shards);

          if(attempt != null) {        /* solution found */
            return grid;
          } else {                     /* solution not found, try again */
            layer--;
            grid.removeShard(x, y);
          }
        }
      }
    }
    shards.add(0, next);
    return null;                       /* the piece can't be placed anywhere */
  }

  public static ShardGrid genFog(ShardGrid grid) {
    for(int y = 1; y < grid.height() - 1; y++) {
      for(int x = 1; x < grid.width() - 1; x++) {
        grid.fog.put(x, y, true);
      }
    }
    return grid;
  }

  public static Collection<Shard> genShards(Collection<Shard> fill, int numShards, int lower, int upper) {
    for(; numShards > 0; numShards--) {
      fill.add(genShard(lower, upper));
    }
    return fill;
  }

  public static Shard genShard(int lower, int upper) {
    int numNodes = (int)((random.nextDouble() * (upper - lower + 1)) + lower);
    return genShard(numNodes);
  }

  public static Shard genShard(int numNodes) {
    Shard shard = new Shard();
    Queue q = new LinkedList<Node>();
    q.add(new Node(shard, 0, 0));
    genShard(shard, q, numNodes);
    double chance = random.nextDouble();
    int stat = (int)(random.nextDouble() * (numNodes)) + 1;
    if(chance <= .3333) {
      shard.stats.precision = stat;
      shard.color = new Vector4f(0.5f, 0.7f, 0.7f, 1);
    } else if(chance <= .6666) {
      shard.stats.strength = stat;
      shard.color = new Vector4f(0.5f, 0.7f, 0.5f, 1);
    } else {
      shard.stats.speed = stat;
      shard.color = new Vector4f(0.5f, 0.5f, 0.7f, 1);
    }
    return shard;
  }

  private static void genShard(Shard shard, Queue<Node> q, int count) {
    Node current = q.remove();
    if(shard.nodes.size() >= count) {
      return;
    }
    else if(!shard.nodes.contains(current) && random.nextDouble() <= .3) {
      shard.addNode(current);
      q.add(new Node(shard, current.x, current.y - 1));
      q.add(new Node(shard, current.x + 1, current.y));
      q.add(new Node(shard, current.x, current.y + 1));
      q.add(new Node(shard, current.x - 1, current.y));
    } else {
      q.add(current);
    }
    genShard(shard, q, count);
  }

  public static Shard genHorizontalScionShard() {
    Shard shard = new Shard();
    shard.addNode(new Node(shard, 0, 0));
    shard.addNode(new Node(shard, 1, 0));
    shard.stats.precision = 1;
    shard.stats.strength = 1;
    shard.stats.speed = 1;
    shard.color = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
    return shard;
  }

  public static Shard genVerticalScionShard() {
    Shard shard = new Shard();
    shard.addNode(new Node(shard, 0, 0));
    shard.addNode(new Node(shard, 0, 1));
    shard.stats.precision = 1;
    shard.stats.strength = 1;
    shard.stats.speed = 1;
    shard.color = new Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
    return shard;
  }

  public static ShardGrid genScionGrid(ShardGrid shards) {
    Shard s = genVerticalScionShard();
    shards.placeShard(s, 1, 1);           /*  # # # # # #  */
    s = genVerticalScionShard();          /*  # | - - | #  */
    shards.placeShard(s, 4, 1);           /*  # | | | | #  */
    s = genVerticalScionShard();          /*  # | | | | #  */
    shards.placeShard(s, 1, 3);           /*  # | - - | #  */
    s = genVerticalScionShard();          /*  # # # # # #  */
    shards.placeShard(s, 4, 3);
    s = genVerticalScionShard();
    shards.placeShard(s, 2, 2);
    s = genVerticalScionShard();
    shards.placeShard(s, 3, 2);
    s = genHorizontalScionShard();
    shards.placeShard(s, 2, 1);
    s = genHorizontalScionShard();
    shards.placeShard(s, 2, 4);
    return shards;
  }

  public static int genScion(World world, int x, int y) {
    int scion = genGameUnit(world, x, y, 0.5f, 0.5f, 1.0f, 1.0f);
    world.getMapper(Player.class).create(scion);
    world.getMapper(Ally.class).create(scion);
    ShardGrid shards = world.getMapper(ShardGrid.class).get(scion);
    shards.base.vitality = 8;
    genScionGrid(shards);
    genFog(shards);
    return scion;
  }

  public static int genAlly(World world, int x, int y) {
    int ally = genGameUnit(world, x, y, 0.5f, 1.0f, 0.5f, 1.0f);
    world.getMapper(Ally.class).create(ally);
    return ally;
  }

  public static int genEnemy(World world, int x, int y) {
    int enemy = genGameUnit(world, x, y, 1.0f, 0.5f, 0.5f, 1.0f);
    world.getMapper(Enemy.class).create(enemy);
    ShardGrid shards = world.getMapper(ShardGrid.class).get(enemy);
    genGrid(shards, 3, 5);
    genFog(shards);
    return enemy;
  }

  public static int genGameUnit(World world, int x, int y, float r, float g, float b, float a) {
    int e = world.create();
    Transform pos = world.getMapper(Transform.class).create(e);
    pos.position = new Vector3f(0.0f, 0.0f, 0.0f);
    pos.rotation = new Vector3f(0.0f, 0.0f, 0.0f);
    pos.scale = 1;
    Coords grid = world.getMapper(Coords.class).create(e);
    grid.move(x, y);
    Mesh mesh = world.getMapper(Mesh.class).create(e);
    Gen.genPyramid(mesh, r, g, b, a);
    Selectable select = world.getMapper(Selectable.class).create(e);
    select.radius = 0.75f;
    Stats stats = world.getMapper(Stats.class).create(e);
    ShardGrid shards = world.getMapper(ShardGrid.class).create(e);
    shards.size = 6;
    shards.base.vitality = 5;
    shards.base.precision = 1;
    shards.base.strength = 1;
    shards.base.speed = 1;
    shards.total.add(shards.base);
    return e;
  }

  public static Mesh genPyramid(Mesh mesh, float r, float g, float b, float a) {
    mesh.vertices = pyramidVerts;
    mesh.indices = pyramidFaces;
    mesh.colors = new float[] {
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      r,    g,    b,    a
    };
    mesh.mode = GL_TRIANGLES;
    return mesh;
  }

  public static final float pyramidVerts[] = new float[] {
      -0.5f,  0.5f, 0.0f,
      0.5f, 0.5f, 0.0f,
      0.5f, -0.5f, 0.0f,
      -0.5f,  -0.5f, 0.0f,
      0.0f, 0.0f, 1.0f
  };

  public static final int pyramidFaces[] = new int[] {
      0, 1, 3, // 1/2 bottom face
      3, 1, 2, // 1/2 bottom face
      0, 1, 4,
      1, 2, 4,
      2, 3, 4,
      3, 0, 4
  };
}
