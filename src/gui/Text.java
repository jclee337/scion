import org.lwjgl.nuklear.*;

import static org.lwjgl.nuklear.Nuklear.*;

public class Text extends Widget {
  private String text = "";
  private int align = LEFT;

  public Text(String text, int align) {
    this.text = text;
    this.align = align;
  }

  public void set(String text) {
    this.text = text;
  }

  protected void draw(NkContext ctx) {
    nk_text(ctx, text, align);
  }

  public static final int LEFT = NK_TEXT_LEFT;
  public static final int CENTER = NK_TEXT_CENTERED;
  public static final int RIGHT = NK_TEXT_RIGHT;
}
