import org.lwjgl.nuklear.*;

import java.util.ArrayList;

import static org.lwjgl.nuklear.Nuklear.*;

public class Window {
  private Window parent, left, right;
  private NkPanel layout = NkPanel.create();
  private NkRect bounds;
  private ArrayList<Widget> widgets = new ArrayList<>();

  public Window(Window parent, NkRect bounds) {
    this(parent, bounds, new ArrayList<Widget>());
  }

  public Window(Window parent, NkRect bounds, ArrayList<Widget> widgets) {
    this.parent = parent;
    this.bounds = bounds;
    this.widgets = widgets;
  }

  public void addWidget(Widget widget) {
    widgets.add(widget);
  }

  public void removeWidget(Widget widget) {
    widgets.remove(widget);
  }

  public boolean hasSplit() {
    return left != null || right != null;
  }

  public Window getParent() {
    return parent;
  }

  public Window getLeft() {
    return left;
  }

  public Window getRight() {
    return right;
  }

  public Window getUp() {
    return left;
  }

  public Window getDown() {
    return right;
  }

  public void setBounds(NkRect bounds) {
    this.bounds = bounds;
  }

  public void draw(NkContext ctx, NkCommandBuffer canvas) {
    nk_layout_space_begin(ctx, NK_STATIC, bounds.h(), 1);
    nk_layout_space_push(ctx, bounds);
    if(left != null) left.draw(ctx, canvas);
    if(right != null) right.draw(ctx, canvas);
    for(Widget w : widgets) {
      w.draw(ctx, canvas, bounds);
    }
    nk_layout_space_end(ctx);
  }

  public Window clear() {
    widgets = new ArrayList<>();
    return this;
  }

  public Window splitVertical(double ratio, ArrayList<Widget> drawLeft, ArrayList<Widget> drawRight) {
    ratio -= Math.floor(ratio);
    NkRect leftBounds = nk_rect(0, 0, (float)(bounds.w() * ratio), bounds.h(), NkRect.create());
    NkRect rightBounds = nk_rect(leftBounds.x(), 0, (float)(bounds.w() * (1 - ratio)), bounds.h(), NkRect.create());
    left = new Window(this, leftBounds, drawLeft);
    right = new Window(this, rightBounds, drawRight);
    return clear();
  }

  public Window splitHorizontal(double ratio, ArrayList<Widget> drawUp, ArrayList<Widget> drawDown) {
    ratio -= Math.floor(ratio);
    NkRect upBounds = nk_rect(0, 0, bounds.w(), (float)(bounds.h() * ratio), NkRect.create());
    NkRect downBounds = nk_rect(0, upBounds.y(), bounds.w(), (float)(bounds.h() * (1 - ratio)), NkRect.create());
    left = new Window(this, upBounds, drawUp);
    right = new Window(this, downBounds, drawDown);
    return clear();
  }

  public Window splitUp(double ratio) {
    return splitHorizontal(ratio, new ArrayList<Widget>(), widgets);
  }

  public Window splitDown(double ratio) {
    return splitHorizontal(1 - (ratio - Math.floor(ratio)), widgets, new ArrayList<Widget>());
  }

  public Window splitLeft(double ratio) {
    return splitVertical(ratio, new ArrayList<Widget>(), widgets);
  }

  public Window splitRight(double ratio) {
    return splitVertical(1 - (ratio - Math.floor(ratio)), widgets, new ArrayList<Widget>());
  }

  public int x() {
    return Math.round(bounds.x());
  }

  public int y() {
    return Math.round(bounds.y());
  }

  public int width() {
    return Math.round(bounds.w());
  }

  public int height() {
    return Math.round(bounds.h());
  }
}
