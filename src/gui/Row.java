import org.lwjgl.nuklear.*;

import java.util.ArrayList;

import static org.lwjgl.nuklear.Nuklear.*;

public class Row extends Widget {
  private ArrayList<Widget> widgets = new ArrayList<>();
  private int height = 10;

  public Row(int height) {
    this.height = height;
  }

  public Row(int height, Widget... widgets) {
    this.height = height;
    this.widgets = new ArrayList<Widget>(widgets.length);
    for(Widget w : widgets)
      this.widgets.add(w);
    //this.widgets.addAll(widgets);
  }

  protected void draw(NkContext ctx) {
    nk_layout_row_dynamic(ctx, height, widgets.size());
    for(Widget w : widgets) {
      w.draw(ctx, canvas, bounds);
    }
  }

  public Row addWidget(Widget widget) {
    widgets.add(widget);
    return this;
  }

  public void setHeight(int height) {
    this.height = height;
  }
}
