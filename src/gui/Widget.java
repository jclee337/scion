import org.lwjgl.nuklear.*;

import static org.lwjgl.nuklear.Nuklear.*;

public abstract class Widget {
  protected NkCommandBuffer canvas;
  protected NkRect bounds;
  private NkColor defacto = nk_rgba_f(1.0f, 1.0f, 1.0f, 1.0f, NkColor.create());
  private NkColor color = nk_rgba_f(1.0f, 1.0f, 1.0f, 1.0f, NkColor.create());

  public void draw(NkContext ctx, NkCommandBuffer canvas, NkRect bounds) {
    this.canvas = canvas;
    this.bounds = bounds;
    draw(ctx);
  }

  protected abstract void draw(NkContext ctx);

  public void drawLine(int x1, int y1, int x2, int y2, int thickness, float r, float g, float b, float a) {
    nk_rgba_f(r, g, b, a, color);
    nk_stroke_line(canvas,
    x1 + (int)bounds.x(),
    y1 + (int)bounds.y(),
    x2 + (int)bounds.x(),
    y2 + (int)bounds.y(),
    thickness, color);
  }

  public void drawLine(int x1, int y1, int x2, int y2, int thickness) {
    drawLine(x1, y1, x2, y2, thickness, defacto.r(), defacto.g(), defacto.b(), defacto.a());
  }

  public void drawRect(int x, int y, int width, int height, float r, float g, float b, float a) {

  }
}
