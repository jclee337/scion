import org.lwjgl.nuklear.*;

import java.util.function.BooleanSupplier;

import static org.lwjgl.nuklear.Nuklear.*;

public class Button extends Widget {
  private String label = "";
  private BooleanSupplier predicate = () -> true;
  private Thunk action = Thunk.NOOP;

  public Button(String label, BooleanSupplier predicate, Thunk action) {
    this.label = label;
    this.predicate = predicate;
    this.action = action;
  }

  protected void draw(NkContext ctx) {
    if(nk_button_text(ctx, label) && predicate.getAsBoolean()) {
      action.run();
    }
  }
}
