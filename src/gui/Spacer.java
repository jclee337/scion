import org.lwjgl.nuklear.*;

import static org.lwjgl.nuklear.Nuklear.*;

public class Spacer extends Widget {

  protected void draw(NkContext ctx) {
    nk_spacing(ctx, 1);
  }
}
