import org.joml.Vector2i;
import org.joml.Vector4f;
import java.util.List;
import java.util.ArrayList;

public class Shard {
  public List<Node> nodes = new ArrayList<>();
  public Vector4f color = new Vector4f(0.70f, 0.70f, 0.70f, 1);
  public Stats stats = new Stats();
  public int health = 0;

  public void addNode(Node n) {
    nodes.add(n);
    health++;
  }
}
