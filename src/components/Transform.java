import com.artemis.Component;
import org.joml.Vector3f;

public class Transform extends Component {
  public Vector3f position = new Vector3f();
  public float scale = 1;
  public Vector3f rotation = new Vector3f();

  public Transform() {}

  public Transform(Transform t) {
    this.position = new Vector3f(t.position);
    this.scale = t.scale;
    this.rotation = new Vector3f(t.rotation);
  }
}
