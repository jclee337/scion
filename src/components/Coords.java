import com.artemis.Component;
import java.util.List;
import java.util.ArrayList;

/**
 *
 */
public class Coords extends Component {
  private int x, y;
  private Grid<Integer> grid;
  private List<EntityProcess> onMoveDo = new ArrayList<>();

  public Coords() {
    this.x = 0;
    this.y = 0;
  }

  public Coords(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int x() {
    return x;
  }

  public int y() {
    return y;
  }

  public Grid<Integer> grid() {
    return grid;
  }

  public boolean move(int x, int y) {
    if(grid != null) {
      if(grid.has(this.x, this.y)) {
        if(grid.has(x, y)) {
          return false;
        } else {
          grid.move(this.x, this.y, x, y);
          this.x = x;
          this.y = y;
          for(EntityProcess p : onMoveDo) {
            p.process(grid.get(x, y));
          }
        }
      }
    }
    this.x = x;
    this.y = y;
    return true;
  }

  public boolean place(Grid<Integer> grid) {
    if(this.grid != null) {
      // TODO: replace
    }
    this.grid = grid;
    return true;
  }

  public void onMoveDo(EntityProcess p) {
    onMoveDo.add(p);
  }

  protected List<Integer> getNeighborsInRange(int range) {
    List<Integer> neighbors = new ArrayList<Integer>();
    for(int sx = x - range; sx <= x + range; sx++) {
      for(int sy = y - range; sy <= y + range; sy++) {
        if((sx != x || sy != y) && !grid.isEmpty(sx, sy)) {
          neighbors.add(grid.get(sx, sy));
        }
      }
    }
    return neighbors;
  }

  public String toString() {
    return "(" + x + ", " + y + ")";
  }
}
