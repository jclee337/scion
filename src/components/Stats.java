import com.artemis.Component;

public class Stats extends Component {
  public int vitality = 0;
  public int precision = 0;
  public int strength = 0;
  public int speed = 0;
  public double exploration = Math.random();
  public double aggression = Math.random();

  public void add(Stats other) {
    this.vitality += other.vitality;
    this.precision += other.precision;
    this.strength += other.strength;
    this.speed += other.speed;
  }

  public void sub(Stats other) {
    this.vitality -= other.vitality;
    this.precision -= other.precision;
    this.strength -= other.strength;
    this.speed -= other.speed;
  }
}
