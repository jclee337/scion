import com.artemis.Component;

public class Mesh extends Component {
  public float[] vertices;
  public float[] colors;
  public int[] indices;
  public int vaoID;
  public int verticesVbo, colorsVbo, indicesVbo;
  public int mode;
}
