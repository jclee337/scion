import com.artemis.Component;
import org.lwjgl.BufferUtils;
import org.lwjgl.nuklear.*;
import org.lwjgl.system.MemoryStack;

import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.nuklear.Nuklear.*;
import static org.lwjgl.system.MemoryStack.*;

public class ShardGrid extends Component {
  public Grid<Node> grid = new Grid<Node>();
  public Grid<Boolean> fog = new Grid<Boolean>(false);
  public List<Shard> list = new ArrayList<>();
  public int size = 6;
	public NkColor background;
  public Stats base = new Stats();
  public Stats total = new Stats();
  public int health = 0;
  public boolean visible = false;

  public ShardGrid() {

  }

  public int width() {
    return size;
  }

  public int height() {
    return size;
  }

  public boolean inBounds(int x, int y) {
    return x >= 0 && x < size && y >= 0 && y < size;
  }

  public boolean canPlace(Shard shard, int x, int y) {
    for(Node n : shard.nodes) {
      if(!inBounds(x + n.x, y + n.y) || !grid.isEmpty(x + n.x, y + n.y)) {
        return false;
      }
    }
    return true;
  }

  public void placeShard(Shard shard, int x, int y) {
    list.add(shard);
    for(Node n : shard.nodes) {
      grid.put(x + n.x, y + n.y, n);
    }
    total.add(shard.stats);
    health = Math.min(total.vitality, numShardsEnabled());
  }

  public Shard removeShard(int x, int y) {
    Node node = grid.get(x, y);
    Shard shard = node.shard;
    list.remove(shard);
    // get the position of the central node
    x -= node.x;
    y -= node.y;
    for(Node n : shard.nodes) {
      grid.remove(x + n.x, y + n.y);
    }
    total.sub(shard.stats);
    health = Math.min(total.vitality, numShardsEnabled());
    return shard;
  }

  /**
   * Enables the node at (x, y) and revives the shard if it was dead.
   */
  public void enable(int x, int y) {
    if(grid.has(x, y) && !grid.get(x, y).enabled) {
      Shard s = grid.get(x, y).shard;

      /* revive the shard if it was dead */
      if(s.health == 0) {
        health++;
        total.add(s.stats);
      }

      grid.get(x, y).enabled = true;
      s.health++;
    }
  }

  /**
   * Disables the node at (x, y) and kills the shard if all nodes are disabled.
   */
  public void disable(int x, int y) {
    if(grid.has(x, y) && grid.get(x, y).enabled) {
      Shard s = grid.get(x, y).shard;

      grid.get(x, y).enabled = false;
      s.health--;

      /* kill the shard if it was alive */
      if(s.health == 0) {
        health--;
        total.sub(s.stats);
      }
    }
  }

  private BFS<Boolean> disableFog = BFS.visitEverythingWithinDistance(1)
  .forEachGoalReplaceWith((x, y, b) -> false);
  public void attack(int x, int y) {
    disable(x, y);
    disableFog.startAt(x, y).search(fog);
  }

  public int numShardsEnabled() {
    int num = 0;
    for(Shard s : list) {
      if(s.health > 0) num++;
    }
    return num;
  }

  /**
   * @return true if two nodes are part of the same shard or both empty
   *         false otherwise
   */
  public boolean isSameShard(int x1, int y1, int x2, int y2) {
    return hasNeighbor(x1, y1, x2, y2) &&
           grid.get(x1, y1).shard == grid.get(x2, y2).shard;
  }

  /**
   * A fog boundary is when the fog (visibility) of two nodes is different
   * @return true if the visibility of the two nodes is different
   *         false if the visibility is the same
   */
  public boolean isFogBoundary(int x1, int y1, int x2, int y2) {
    return fog.get(x1, y1) ^ fog.get(x2, y2);
  }

  /**
   * @return true if boundary is between two fogged nodes, false otherwise
   */
  public boolean betweenFog(int x1, int y1, int x2, int y2) {
    return fog.get(x1, y1) && fog.get(x2, y2);
  }

  /**
   * @return true if both are non-empty nodes
   *         false if either one is empty or both are empty
   */
  public boolean hasNeighbor(int x1, int y1, int x2, int y2) {
    return !grid.isEmpty(x1, y1) && !grid.isEmpty(x2, y2);
  }

  /**
   * An empty boundary is when one of two adjacent nodes is empty and the other non-empty
   * @return true if boundary is between empty nodes, false otherwise
   */
  public boolean betweenEmptiness(int x1, int y1, int x2, int y2) {
    return grid.isEmpty(x1, y1) && grid.isEmpty(x2, y2);
  }


}
