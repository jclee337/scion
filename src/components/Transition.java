import com.artemis.Component;
import org.joml.Vector3f;

public class Transition extends Component {
  public Transform origin;
  public Transform goal;
  public float duration;
  public float accumulated = 0;

  public void setBySpeed(float speed) {
    float distance = goal.position.sub(origin.position, new Vector3f()).length();
    this.duration = distance / speed;
  }
}
