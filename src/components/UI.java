import com.artemis.Component;
import org.lwjgl.nuklear.*;

import static org.lwjgl.nuklear.Nuklear.*;

public class UI extends Component {
  private int hidden = NK_WINDOW_HIDDEN;
  private boolean hovered = false;
  private int x = 0, y = 0;
  private int width = 0, height = 0;
  private String title = "Game Window";
  private NkPanel layout = NkPanel.create();
  private NkRect bounds = nk_rect(0, 0, 0, 0, NkRect.create());
  private Window root = new Window(null, nk_rect(0, 0, bounds.w(), bounds.h(), NkRect.create()));

  public void draw(NkContext ctx) {
    nk_begin(ctx, layout, title, bounds,
    NK_WINDOW_TITLE | NK_WINDOW_BORDER | NK_WINDOW_NO_SCROLLBAR | hidden);
    NkCommandBuffer canvas = nk_window_get_canvas(ctx);

    if(hidden == 0) {
      hovered = nk_window_is_hovered(ctx);
      root.draw(ctx, canvas);
    } else {
      hovered = false;
    }

    nk_end(ctx);
  }

  public boolean enabled() {
    return hidden == 0;
  }

  public UI enable() {
    hidden = 0;
    return this;
  }

  public UI disable() {
    hidden = NK_WINDOW_HIDDEN;
    return this;
  }

  public boolean hovered() {
    return hovered;
  }

  public UI move(int x, int y) {
    this.x = x;
    this.y = y;
    nk_rect(x, y, width, height, bounds);
    //root.setBounds(bounds);
    return this;
  }

  /* TODO: complicated, needs more work in the future */
  public UI resize(int width, int height) {
    this.width = width;
    this.height = height;
    nk_rect(x, y, width, height, bounds);
    //root.setBounds(bounds);
    return this;
  }

  public UI setTitle(String title) {
    this.title = title;
    return this;
  }

  public Window getRootWindow() {
    return root;
  }
}
