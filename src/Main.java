import com.artemis.*;
import com.artemis.EntityTransmuter;
import com.artemis.WorldConfigurationBuilder;
import org.joml.Vector3f;
import org.joml.Vector4f;

import java.nio.FloatBuffer;
import java.util.ArrayList;

import static org.lwjgl.opengl.GL11.*;

/**
 * This is the entry point for running the game
 * All source code that isn't in a library was written by the authors below
 * @author Josh Lee
 */
public class Main {

  private World world;
  private double delta = 16.6666667; // milliseconds per frame

  public static void main(String[] args) {
    new Main().run();
  }

  public void run() {
    init();
    loop();
  }

  public void init() {
    WorldConfiguration config = new WorldConfigurationBuilder()
      .with(
        new TurnSystem(),
        new GridTurnSystem(),
        new EntitySelectionSystem(),
        new TransitionSystem(),
        new WindowSystem(),
        new CameraSystem(),
        new ArenaSystem(),

        // behavior and gui
        new StatsWindowSystem(),
        new PlayerTurnSystem(),
        new AllyTurnSystem(),
        new EnemyBehaviorSystem(),
        new ShardGridSystem(),
        new CombatSystem(),
        new GridTransformSystem(),

        // rendering
        new RenderSystem(),
        new GUIRenderSystem(),

        // meta
        new InputSystem(),
        new TimerSystem()
      ).build();
    world = new World(config);

    // create player
    int e = Gen.genScion(world, 0, 0);
    Inventory invent = world.getMapper(Inventory.class).create(e);
    invent.shards = new ArrayList<Shard>();
    Gen.genShards(invent.shards, 40, 2, 2);
  }

  public void loop() {
    long current = System.nanoTime();
    double accumulator = 0;
    while(true) {
      long start = System.nanoTime();
      long frame = start - current;
      current = start;

      accumulator += (frame / 1e6);

      while(accumulator >= delta) {
        world.setDelta((float)delta);
        world.process();
        accumulator -= delta;
      }
    }
  }
}
