
/**
 * A simple interface to represent lambdas which
 * take no input and give no output.
 */
@FunctionalInterface
public interface Thunk {
  public void run();
  public static final Thunk NOOP = () -> {};
}
