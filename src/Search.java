import org.joml.Vector2i;

import java.util.Collection;
import java.util.LinkedList;

public abstract class Search<S extends Search<S, T>, T> {

  protected Vector2i start;
  protected Vector2i min, max;
  protected Grid.Predicate<T> isWithinArea =
  (x, y, t) -> min == null || max == null ||
  Grid.withinArea(x, y, min.x, min.y, max.x, max.y);
  protected Collection<Vector2i> branchDirections = Grid.ORTHO;
  protected int maxDistance = -1;
  protected Grid.Predicate<T> isWithinDistance =
  (x, y, t) -> maxDistance < 0 ||
  Grid.manhattan(start.x, start.y, x, y) <= maxDistance;
  protected Grid.Predicate<T> shouldVisit;
  protected Grid.Predicate<T> isGoal;
  protected Grid.Function<T> replaceGoalWith = (x, y, t) -> t;
  protected int maxNumGoals = 0; // less than one means unlimited goals
  protected T defaultValue; // internal use

  protected abstract S getThis();

  public S startAt(int x, int y) {
    this.start.set(x, y);
    return getThis();
  }

  public S startAt(Vector2i v) {
    return startAt(v.x, v.y);
  }

  public S searchInArea(int x1, int y1, int x2, int y2) {
    min = new Vector2i(x1, y1);
    max = new Vector2i(x2, y2);
    return getThis();
  }

  public S searchBranchDirections(Collection<Vector2i> branchDirections) {
    this.branchDirections = branchDirections;
    return getThis();
  }

  public S searchWithinDistance(int distance) {
    this.maxDistance = distance;
    return getThis();
  }

  public S searchInfiniteDistance() {
    return searchWithinDistance(-1);
  }

  public S returnAtMost(int numGoals) {
    this.maxNumGoals = numGoals;
    return getThis();
  }

  public S returnFirstGoal() {
    return returnAtMost(1);
  }

  public S returnAllGoals() {
    return returnAtMost(0);
  }

  public S visitIf(Grid.Predicate<T> visitIf) {
    this.shouldVisit = visitIf;
    return getThis();
  }

  public S visitEverything() {
    return visitIf((x, y, t) -> true);
  }

  public S visitNonEmpty() {
    return visitIf((x, y, t) -> t != null && t != this.defaultValue);
  }

  public Collection<Vector2i> search(Grid<T> grid) {
    return search(grid, new LinkedList<Vector2i>());
  }

  public Collection<Vector2i> search(Grid<T> grid, Collection<Vector2i> fill) {
    Grid<Boolean> visited = new Grid<Boolean>(false);
    this.defaultValue = grid.getDefault();

    /* form final neighbor searching predicate */
    Grid.Predicate<T> visitNeighborIf = shouldVisit
    .and((x, y, t) -> !visited.get(x, y) && grid.withinBounds(x, y))
    .and(isWithinDistance)
    .and(isWithinArea);

    run(grid, fill, visited, visitNeighborIf);

    return fill;
  }

  protected abstract void run(Grid<T> grid, Collection<Vector2i> fill,
  Grid<Boolean> visited, Grid.Predicate<T> visitNeighborIf);
}
