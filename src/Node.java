

public class Node {
  public Shard shard;
  public int x, y;
  public boolean enabled = true;

  public Node(Shard shard, int x, int y) {
    this.shard = shard;
    this.x = x;
    this.y = y;
  }

  @Override
  public boolean equals(Object o) {
    if(o instanceof Node) {
      Node n = (Node)o;
      return x == n.x && y == n.y;
    }
    return false;
  }
}
