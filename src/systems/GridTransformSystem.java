import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IteratingSystem;
import org.joml.Vector2i;
import org.joml.Vector3f;

import static org.lwjgl.opengl.GL11.*;

public class GridTransformSystem extends IteratingSystem {

  private ComponentMapper<Coords> mGridSpace;
  private ComponentMapper<Transform> mTform;
  private int grid;

  public GridTransformSystem() {
    super(Aspect.all(Coords.class, Transform.class).exclude(Invisible.class));
  }

  @Override
  public void initialize() {
    initGrid();
  }

  @Override
  public void inserted(int e) {
    float cell = 1.5f;
    Coords grid = mGridSpace.get(e);
    Transform pos = mTform.get(e);
    pos.position.x = grid.x() * cell;
    pos.position.y = grid.y() * cell;
    pos.scale = 0;
    Transition trans = world.getMapper(Transition.class).create(e);
    trans.goal = new Transform(pos);
    trans.goal.scale = 1;
    trans.duration = 200;
  }

  @Override
  public void process(int e) {

  }

  @Override
  public void removed(int e) {
    Coords grid = mGridSpace.get(e);
    Transform pos = mTform.get(e);
    pos.scale = 1;
    Transition trans = world.getMapper(Transition.class).create(e);
    trans.goal = new Transform(pos);
    trans.goal.scale = 0;
    trans.duration = 200;
  }

  public void initGrid() {
    float cell = 1.5f;
    int size = 15;
    grid = world.create();
    Transform pos = mTform.create(grid);
    pos.position = new Vector3f(0, 0, 0);
    pos.rotation = new Vector3f(0, 0, 0);
    pos.scale = 1;
    Mesh mesh = world.getMapper(Mesh.class).create(grid);
    // 16 lines * 2 axes * 2 vertices per line * 3 floats per vertex = 192
    int lineCount = size + 1;
    float half = ((lineCount * cell) / 2.0f) - (cell / 2.0f);
    int vertCount = lineCount * 4;
    mesh.vertices = new float[vertCount * 3];
    mesh.colors = new float[vertCount * 4];
    mesh.indices = new int[vertCount];
    for(int i = 0; i < lineCount; i++) {
      int k = i  * 6;
      float end = (i * cell);
      mesh.vertices[k] = end - half;     // vertex 1 x
      mesh.vertices[k + 1] = -half;      // vertex 1 y
      mesh.vertices[k + 2] = 0;          // vertex 1 z
      mesh.vertices[k + 3] = end - half; // vertex 2 x
      mesh.vertices[k + 4] = half;       // vertex 2 y
      mesh.vertices[k + 5] = 0;          // vertex 2 z
    }
    for(int i = 0; i < lineCount; i++) {
      int k = (i * 6) + 96;
      float end = (i * cell);
      mesh.vertices[k] = -half;
      mesh.vertices[k + 1] = end - half;
      mesh.vertices[k + 2] = 0;
      mesh.vertices[k + 3] = half;
      mesh.vertices[k + 4] = end - half;
      mesh.vertices[k + 5] = 0;
    }
    for(int i = 0; i < vertCount; i++) {
      int k = i * 4;
      mesh.colors[k] = 1.0f;
      mesh.colors[k + 1] = 1.0f;
      mesh.colors[k + 2] = 1.0f;
      mesh.colors[k + 3] = 0.5f;
      mesh.indices[i] = i;
    }
    mesh.mode = GL_LINES;
  }
}
