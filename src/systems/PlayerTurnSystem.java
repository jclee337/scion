import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySubscription;
import com.artemis.systems.IteratingSystem;
import com.artemis.utils.IntBag;
import org.joml.Vector2i;
import org.lwjgl.nuklear.*;
import org.lwjgl.system.MemoryStack;

import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class PlayerTurnSystem extends IteratingSystem {

  private WindowSystem window;
  private ComponentMapper<Coords> mGrid;
  private ComponentMapper<ShardGrid> mShards;
  private Bindings bindings;
  private UI ui;
  private Integer turn;
  private Integer selected;
  private Vector2i turnPos = new Vector2i();
  private Aspect target;
  private boolean attacked = false;

  public PlayerTurnSystem() {
    super(Aspect.all(Player.class, ShardGrid.class, Coords.class, Controllable.class));
  }

  @Override
  public void initialize() {
    super.initialize();
    target = Aspect.all(Enemy.class, ShardGrid.class, Coords.class, Selected.class).build(world);

    /* Listen for damaged enemies */
    Utils.listener(world, Aspect.all(Damaged.class),
    (int inserted) -> {
      attacked = true;
      world.getMapper(Damaged.class).remove(inserted);
    },
    removed -> {});

    /* Listen for selection of tiles for movement */
    Utils.listener(world, Aspect.all(Coords.class, Transform.class, Selected.class).exclude(ShardGrid.class),
    (int inserted) -> {
      if(turn != null) {
        Coords pos = world.getMapper(Coords.class).get(turn);
        Coords sel = world.getMapper(Coords.class).get(inserted);
        int distx = Math.abs(sel.x() - turnPos.x);
        int disty = Math.abs(sel.y() - turnPos.y);
        if(pos.grid().isEmpty(sel.x(), sel.y()) && distx <= 1 && disty <= 1) {
          pos.move(sel.x(), sel.y());
        }
      }
    },
    removed -> {});

    bindings = new Bindings(world);

    /* Catch and consume mouse clicks */
    bindings.put(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_PRESS, Thunk.NOOP);
    bindings.mode = Bindings.Mode.DEFERRING;

    ui = world.getMapper(UI.class).create(world.create());
    ui.setTitle("Scion Actions")
    .move(window.getWidth() - 250, window.getHeight() - 250)
    .resize(250, 250);
    ui.getRootWindow().addWidget(
      new Row(40,
        new Button("Attack",
        () -> selected != null && !attacked,
        () -> {
          Stats stats = mShards.get(turn).total;
          Attacked a = world.getMapper(Attacked.class).create(selected);
          a.attacker = stats;
        })));
    ui.getRootWindow().addWidget(
      new Row(40,
        new Spacer(),
        new Button("End Turn",
        () -> true,
        () -> {
          world.getMapper(Controllable.class).remove(turn);
          if(selected != null) {
            world.getMapper(Attacked.class).remove(selected);
          }
        }))
    );
    ui.disable();
  }

  @Override
  public void inserted(int e) {
    Coords pos = mGrid.get(e);
    Stats stats = mShards.get(e).total;
    ui.enable();
    attacked = false;
    turn = e;
    Coords c = world.getMapper(Coords.class).get(e);
    turnPos.set(c.x(), c.y());
  }

  @Override
  public void process(int e) {
    Coords pos = mGrid.get(e);

    if(ui.hovered()) {
      bindings.enable();
    } else if(bindings.enabled()) {
      bindings.disable();
    }

    List<Integer> neighbors = pos.getNeighborsInRange(1);
    selected = null;
    for(Integer neighbor : neighbors) {
      if(target.isInterested(world.getEntity(neighbor))) {
        selected = neighbor;
      }
    }
  }

  @Override
  public void removed(int e) {
    ui.disable();
    turn = null;
    selected = null;
  }
}
