import com.artemis.BaseEntitySystem;
import com.artemis.BaseSystem;
import com.artemis.Entity;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @deprecated
 * Bit of a dumb, convoluted way to solve some minor problems.
 * Keeping this around for a rainy day.
 */
@Deprecated
public class EventSystem extends BaseSystem {

  private Map<Class<? extends Event>, List<Handler>> handlers;
  private List<PendingEvent> pending;

  public EventSystem() {
    handlers = new HashMap<>();
    pending = new LinkedList<>();
  }

  @Override
  public void initialize() {
    /*
    for(BaseSystem system : world.getSystems()) {
      register(system);
    }
    */
  }

  @Override
  public boolean checkProcessing() {
    return !pending.isEmpty();
  }

  @Override
  public void processSystem() {
    /*  Blah blah legacy crap */

    /* locking the size avoids the processing of events
     * that are queued during this batch of events, so there is
     * always a 1 frame delay between when an event is queued
     * and when it's processed
     */
    /*
    int size = pending.size();
    for(int i = 0; i < size; i++) {
      PendingEvent e = pending.remove(0);
      if(e.entity != null) {
        send(e.event, e.entity);
      } else {
        send(e.event);
      }
    }
    */
  }

  public void queue(Event event, Entity entity) {
    //pending.add(new PendingEvent(event, entity));
  }

  public void queue(Event event) {
    //pending.add(new PendingEvent(event));
  }

  /*
  @SuppressWarnings("unchecked")
  public void send(Event event, Entity entity) {
    if(handlers.containsKey(event.getClass())) {
      for(Handler handler : handlers.get(event.getClass())) {
        if(handler.method.getParameterCount() == 1) {
          invoke(handler, event);
        } else if(handler.system instanceof BaseEntitySystem) {
          BaseEntitySystem system = (BaseEntitySystem)handler.system;
          if(system.getSubscription().getAspect().isInterested(entity)) {
            invoke(handler, event, entity);
          }
        } else {
          invoke(handler, event, entity);
        }
      }
    }
  }

  public void send(Event event) {
    if(handlers.containsKey(event.getClass())) {
      for(Handler handler : handlers.get(event.getClass())) {
        if(handler.method.getParameterCount() == 1) {
          invoke(handler, event);
        }
      }
    }
  }

  private void invoke(Handler handler, Event event, Entity entity) {
    try {
      handler.method.invoke(handler.system, event, entity);
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  private void invoke(Handler handler, Event event) {
    try {
      handler.method.invoke(handler.system, event);
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public void register(BaseSystem system) {
    Class klass = system.getClass();
    for(Method method : klass.getMethods()) {
      if(method.getAnnotation(ReceiveEvent.class) != null) {
        Class<? extends Event> event = (Class<? extends Event>)method.getParameterTypes()[0];
        if(handlers.containsKey(event)) {
          handlers.get(event).add(new Handler(system, method));
        } else {
          List list = new ArrayList<Handler>();
          list.add(new Handler(system, method));
          handlers.put(event, list);
        }
      }
    }
  }

  public void unregister() {

  }
  */

  private class Handler {

    private BaseSystem system;
    private Method method;

    public Handler(BaseSystem system, Method method) {
      this.system = system;
      this.method = method;
    }
  }

  private class PendingEvent {

    private Event event;
    private Entity entity;

    public PendingEvent(Event event, Entity entity) {
      this.event = event;
      this.entity = entity;
    }

    public PendingEvent(Event event) {
      this.event = event;
      this.entity = null;
    }
  }
}
