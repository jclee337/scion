import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import org.joml.Vector2i;
import org.lwjgl.nuklear.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nuklear.Nuklear.*;

public class CombatSystem extends IteratingSystem {

  private WindowSystem window;
  private GUIRenderSystem gui;
  private ComponentMapper<ShardGrid> mShards;
  private ComponentMapper<Stats> mStats;
  private ComponentMapper<Attacked> mAttacked;
  private Bindings bindings;
  private int hidden = NK_WINDOW_HIDDEN;
  private int subject = 0; // ID of entity being attacked
  private int padding = 20;
  private int gridSize = 60;
  private NkPanel layout, action;
  private NkRect bounds;
  private NkRect gridBounds;
  private NkRect actionBounds;
  private NkInput input;
  private Grid<Boolean> selectedNodes;
  private Grid<Boolean> originNodes;
  private boolean attacked = false;

  public CombatSystem() {
    super(Aspect.all(Attacked.class, ShardGrid.class, Coords.class));
  }

  public void initialize() {
    layout = NkPanel.create();
    action = NkPanel.create();
    bounds = NkRect.create();
    gridBounds = NkRect.create();
    actionBounds = NkRect.create();

    bindings = new Bindings(world);
    bindings.mode = Bindings.Mode.DEFERRING;
    bindings.put(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_PRESS, () -> {
      if(!attacked) {
        ShardGrid shards = mShards.get(subject);
        Stats stats = mAttacked.get(subject).attacker;
        /* calculate grid cell position of mouse */
        int x = (int)Math.floor((bindings.mousex - bounds.x() - padding) / gridSize);
        int y = (int)Math.floor((bindings.mousey - bounds.y() - padding) / gridSize);
        if(shards.inBounds(x, y)) {
          boolean isSelected = selectedNodes.get(x, y);

          /* toggle the marking of the node */
          if(isSelected) {
            deselectNode(shards, x, y);
          } else {
            selectNode(shards, stats, x, y);
          }
        }
      }
    });
    bindings.put(GLFW_KEY_ESCAPE, 0, GLFW_PRESS, () -> {
      mAttacked.remove(subject);
    });
  }

  public void enableInput() {
    bindings.enable();
  }

  public void disableInput() {
    bindings.disable();
  }

  public void inserted(int e) {
    selectedNodes = new Grid<Boolean>(false);
    originNodes = new Grid<Boolean>(false);
    removed(subject);
    hidden = 0;
    subject = e;
    attacked = false;
    if(world.getMapper(Ally.class).has(e)) {
      double delay = 700;
      int i = 1;
      ShardGrid attacked = mShards.get(e);
      Stats attacker = world.getMapper(Attacked.class).get(e).attacker;
      Collection<Vector2i> best = findBestAttack(attacked, attacker);
      for(Vector2i v : best) {
        TimerSystem.timer(world, delay * i,
        () -> {
          selectNode(attacked, attacker, v.x, v.y);
        });
        i++;
      }
      i++;
      TimerSystem.timer(world, delay * i, () -> {
        processAttack(attacked);
        world.getMapper(Damaged.class).create(e);
        originNodes = new Grid<Boolean>(false);
        selectedNodes = new Grid<Boolean>(false);
        this.attacked = true;
        enableInput();
      });
    } else {
      enableInput();
    }
  }

  public void begin() {
    nk_begin(gui.ctx, layout, "Attacking Shard Grid",
    nk_rect(300, 100, 700, 500, bounds),
    NK_WINDOW_TITLE | NK_WINDOW_BORDER | hidden);
  }

  public void process(int e) {
    NkInput in = gui.ctx.input();
    ShardGrid shards = mShards.get(e);
    Stats stats = mAttacked.get(e).attacker;
    // canvas is where drawing instructions are sent
    NkCommandBuffer canvas = nk_window_get_canvas(gui.ctx);
    boolean done = false;

    /* calculate window and submenu dimensions */
    nk_window_get_content_region(gui.ctx, bounds);
    int limiter = (int)Math.min(bounds.w() * 2 / 3, bounds.h());
    int gridLength = (limiter - (padding * 2));
    gridSize = gridLength / shards.width();
    nk_rect(bounds.x() + (bounds.w() / 3) - (gridLength / 2),
    bounds.y() + (bounds.h() / 2) - (gridLength / 2),
    gridLength, gridLength, gridBounds);
    nk_rect((bounds.w() * 2 / 3), -5,
    bounds.w() / 3, bounds.h(), actionBounds);

    /* calculate grid cell position of mouse */
    //int x = (int)Math.floor((c.mousex - bounds.x() - padding) / gridSize);
    //int y = (int)Math.floor((c.mousey - bounds.y() - padding) / gridSize);

    /* draw the grid */
    gui.drawStatsGrid(shards, canvas, gridBounds, true);

    NkColor red = NkColor.create();
    nk_rgba_f(0.6f, 0.0f, 0.0f, 1.0f, red);
    for(int nx = 0; nx < shards.width(); nx++) {
      int gx = Math.round(gridBounds.x()) + (nx * gridSize);
      for(int ny = 0; ny < shards.height(); ny++) {
        int gy = Math.round(gridBounds.y()) + (ny * gridSize);
        if(selectedNodes.get(nx, ny)) {
          nk_stroke_line(canvas, gx, gy, gx + gridSize, gy + gridSize, 2, red);
          nk_stroke_line(canvas, gx, gy + gridSize, gx + gridSize, gy, 2, red);
        }
        if(originNodes.get(nx, ny)) {
          nk_stroke_line(canvas, gx, gy + (gridSize / 2), gx + gridSize, gy + (gridSize / 2), 2, red);
          nk_stroke_line(canvas, gx + (gridSize / 2), gy, gx + (gridSize / 2), gy + gridSize, 2, red);
        }
      }
    }

    /* draw the attack submenu */
    nk_layout_space_begin(gui.ctx, NK_STATIC, bounds.h(), 1);
    nk_layout_space_push(gui.ctx, actionBounds);
    if(nk_group_begin(gui.ctx, action, "Actions", NK_WINDOW_BORDER)) {
      nk_layout_row_dynamic(gui.ctx, 50, 1);
      if(attacked) {
        nk_label(gui.ctx, "Attack Complete", NK_TEXT_CENTERED);
      } else if(originNodes.count() <= 0) {
        nk_label(gui.ctx, "Select Nodes", NK_TEXT_CENTERED);
      } else {
        if(nk_button_text(gui.ctx, "Confirm Attack")) {
          attacked = true;
          processAttack(shards);
          world.getMapper(Damaged.class).create(e);
          originNodes = new Grid<Boolean>(false);
          selectedNodes = new Grid<Boolean>(false);
        }
      }
      nk_group_end(gui.ctx);
    }
    nk_layout_space_end(gui.ctx);

  }

  public void end() {
    nk_end(gui.ctx);
  }

  public void removed(int e) {
    hidden = NK_WINDOW_HIDDEN;
    mAttacked.remove(e);
    disableInput();

    /* kill the unit, if necessary */
    if(mShards.has(e) && mShards.get(e).health <= 0) {
      world.getMapper(Coords.class).remove(e);
      world.getMapper(Selectable.class).remove(e);
      world.getMapper(Selected.class).remove(e);
    }

    subject = 0;
  }

  Grid.Predicate<Boolean> nodeIsSelected = (x, y, b) -> selectedNodes.get(x, y);
  Grid.Predicate<Boolean> nodeIsOrigin = (x, y, b) -> originNodes.get(x, y);

  /**
  * Attempts to select the node at (x, y).
  */
  public void selectNode(ShardGrid shards, Stats stats, int x, int y) {
    int maxSelections = Math.min(stats.precision, stats.strength);
    int maxOrigins = Math.min(maxSelections, Math.max(1, stats.precision - stats.strength));
    selectedNodes.put(x, y, true);
    if(originNodes.count() < maxOrigins && selectedNodes.count() < maxSelections && mustBeOrigin(shards, x, y)) {
      originNodes.put(x, y, true);
    } else if(selectedNodes.count() > maxSelections || !isConnectedToOrigin(x, y)) {
      selectedNodes.put(x, y, false);
    }
  }

  /**
   * Deselects the node at (x, y) and reconciles other selected nodes that
   * the deselection at (x, y) affects according to the rules below:
   * Any sequence of selected nodes that aren't connected to an origin
   * must become deselected.
   */
  BFS<Boolean> deselect = BFS.forEachContiguous((x, y, b) -> false);
  public void deselectNode(ShardGrid shards, int x, int y) {
    selectedNodes.put(x, y, false);
    originNodes.put(x, y, false);
    for(Vector2i neighbor : selectedNodes.getCoords(x, y, Grid.ORTHO, (xx, yy, b) -> b)) {
      if(!isConnectedToOrigin(neighbor.x, neighbor.y)) {
        deselect.startAt(neighbor.x, neighbor.y).search(selectedNodes);
      }
    }
  }

  BFS<Boolean> findConnectedOrigin = BFS.findContiguous(nodeIsOrigin);
  public boolean isConnectedToOrigin(int x, int y) {
    return findConnectedOrigin.startAt(x, y).someSuchNode(selectedNodes);
  }

  /**
  * Assuming that the node at coords (x, y) will be selected, this method
  * returns whether that node will have to be an origin node or not.
  * @return true if the node (x, y) is forced to be an attack origin
  */
  public boolean mustBeOrigin(ShardGrid shards, int x, int y) {
    return !shards.fog.get(x, y) && !selectedNodes.some(x, y, Grid.ORTHO, (xx, yy, b) -> b);
  }

  BFS<Boolean> findOrigins = BFS.searchEverything(); // returns all true coords
  BFS<Boolean> attack = BFS.findAllContiguous();
  public void processAttack(ShardGrid shards) {
    attack.forEachGoalReplaceWith((x, y, b) -> {
      shards.attack(x, y);
      return false;
    });
    findOrigins.forEachGoalReplaceWith((x, y, b) -> {
      attack.startAt(x, y).search(selectedNodes);
      return false;
    });
    originNodes.search(findOrigins);
  }

  public Collection<Vector2i> findBestAttack(ShardGrid attacked, Stats attacker) {
    double bestScore = 0;
    ArrayList<Collection<Vector2i>> bestAttacks = new ArrayList<>();
    int range = Math.min(attacker.precision, attacker.strength);
    for(int x = 0; x < attacked.width(); x++) {
      for(int y = 0; y < attacked.height(); y++) {
        if(!attacked.fog.get(x, y)) {
          for(Collection<Vector2i> attack : findAllAttacksAt(attacked, new Vector2i(x, y), range)) {
            double score = scoreAttack(attacked, attacker, attack);
            if(score == bestScore) {
              bestAttacks.add(attack);
            } else if(score > bestScore || bestAttacks.isEmpty()) {
              bestScore = score;
              bestAttacks.clear();
              bestAttacks.add(attack);
            }
          }
        }
      }
    }
    return bestAttacks.get((int)(Math.random() * bestAttacks.size()));
  }

  public LinkedList<LinkedList<Vector2i>> findAllAttacksAt(
  ShardGrid attacked, Vector2i node, int range) {
    return findAllAttacksAt(attacked,
    new LinkedList<Vector2i>(),
    new LinkedList<Vector2i>(),
    new Grid<Boolean>(false),
    new Grid<Boolean>(false),
    node, range, new LinkedList<LinkedList<Vector2i>>());
  }

  public LinkedList<LinkedList<Vector2i>> findAllAttacksAt(ShardGrid attacked,
  LinkedList<Vector2i> path, LinkedList<Vector2i> tentative,
  Grid<Boolean> isInPath, Grid<Boolean> exhausted,
  Vector2i node, int range, LinkedList<LinkedList<Vector2i>> fill) {
    path.add(node);
    isInPath.put(node, true);

    if(range <= 1) {
      fill.add(path);
      return fill;
    }

    tentative.addAll(isInPath.getCoords(node.x, node.y, Grid.ORTHO, new LinkedList<Vector2i>(),
    (x, y, b) -> !b && !exhausted.get(x, y) && attacked.inBounds(x, y)));

    LinkedList<Vector2i> best = path;
    while(!tentative.isEmpty()) {
      Vector2i current = tentative.poll();
      findAllAttacksAt(attacked,
      new LinkedList<>(path),
      new LinkedList<>(tentative),
      new Grid<>(isInPath),
      new Grid<>(exhausted),
      current, range - 1, fill);
      exhausted.put(current, true);
    }
    return fill;
  }

  public double scoreAttack(ShardGrid attacked, Stats attacker, Collection<Vector2i> attack) {
    double score = 0;
    for(Vector2i v : attack) {
      if(attacked.fog.get(v)) {
        score += attacker.exploration;
      } else if(!attacked.grid.isEmpty(v) && attacked.grid.get(v).enabled) {
        score += attacker.aggression;
      }
    }
    return score;
  }

}
