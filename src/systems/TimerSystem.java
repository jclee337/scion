import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.World;
import com.artemis.systems.DelayedIteratingSystem;

public class TimerSystem extends DelayedIteratingSystem {

  private ComponentMapper<Timer> mTimer;

  public TimerSystem() {
    super(Aspect.all(Timer.class));
  }

  @Override
  public void processDelta(int e, float delta) {
    mTimer.get(e).time -= delta;
  }

  @Override
  public float getRemainingDelay(int e) {
    return (float)mTimer.get(e).time;
  }

  @Override
  public void processExpired(int e) {
    Timer t = mTimer.get(e);
    t.thunk.run();
    mTimer.remove(e);
  }

  public static void timer(World world, double time, Thunk thunk) {
    int e = world.create();
    Timer t = world.getMapper(Timer.class).create(e);
    t.time = time;
    t.thunk = thunk;
  }
}
