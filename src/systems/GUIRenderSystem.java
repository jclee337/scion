import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IteratingSystem;
import org.joml.Vector4i;
import org.lwjgl.BufferUtils;
import org.lwjgl.nuklear.*;
import org.lwjgl.stb.*;
import org.lwjgl.system.*;
import org.lwjgl.system.MemoryStack;
import org.lwjgl.system.Platform;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.BufferUtils.*;
import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nuklear.Nuklear.*;
import static org.lwjgl.opengl.ARBDebugOutput.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.opengl.GL43.*;
import static org.lwjgl.stb.STBTruetype.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

/**
 * Don't go into this file. You've been warned.
 */
public class GUIRenderSystem extends IteratingSystem {

  private static final int NK_BUFFER_DEFAULT_INITIAL_SIZE = 4 * 1024;

  private static final int MAX_VERTEX_BUFFER  = 512 * 1024;
  private static final int MAX_ELEMENT_BUFFER = 128 * 1024;

  private static final NkAllocator ALLOCATOR;

  static {
    ALLOCATOR = NkAllocator.create();
    ALLOCATOR.alloc((handle, old, size) -> {
      long mem = nmemAlloc(size);
      if(mem == NULL) {
        throw new OutOfMemoryError();
      }
      return mem;
    });
    ALLOCATOR.mfree((handle, ptr) -> nmemFree(ptr));
  }

  private WindowSystem window;
  private long win;
  private int width, height;
  private int display_width, display_height;
  private Test test = new Test();
  public final NkContext ctx = NkContext.create();
  public final NkUserFont default_font = NkUserFont.create();
  private NkBuffer cmds = NkBuffer.create();
  private NkDrawNullTexture null_texture = NkDrawNullTexture.create();
  private ByteBuffer ttf;
  private Bindings bindings;
  private NkColor gray = NkColor.create();
  private NkColor lightGray = NkColor.create();
  private NkColor darkGray = NkColor.create();
  private NkColor clear = NkColor.create();

  private int vbo, vao, ebo;
  private ShaderProgram shaders;
  private int prog;
  private int uniform_tex;
  private int uniform_proj;

  public GUIRenderSystem() {
    super(Aspect.all(UI.class));
  }

  @Override
  public void initialize() {
    try {
      this.ttf = Utils.ioResourceToByteBuffer("fonts/OpenSans.ttf", 160 * 1024);
    } catch(IOException e) {
      e.printStackTrace();
    }
    win = window.getWindow();
    glfwSetScrollCallback(win, (window, xoffset, yoffset) -> nk_input_scroll(ctx, (float)yoffset));
    glfwSetCharCallback(win, (window, codepoint) -> nk_input_unicode(ctx, codepoint));

    nk_init(ctx, ALLOCATOR, null);
    ctx.clip().copy((handle, text, len) -> {
      if(len == 0) {
        return;
      }
      try(MemoryStack stack = stackPush()) {
        ByteBuffer str = stack.malloc(len + 1);
        memCopy(text, memAddress(str), len);
        str.put(len, (byte)0);

        glfwSetClipboardString(win, str);
      }
    });
    ctx.clip().paste((handle, edit) -> {
      long text = nglfwGetClipboardString(win);
      if(text != NULL) {
        nnk_textedit_paste(edit, text, nnk_strlen(text));
      }
    });

		nk_buffer_init(cmds, ALLOCATOR, NK_BUFFER_DEFAULT_INITIAL_SIZE);
    try {
      shaders = new ShaderProgram();
      shaders.createVertexShader(Utils.loadResource("/shaders/gui.vert"));
      shaders.createFragmentShader(Utils.loadResource("/shaders/gui.frag"));
      shaders.link();
      prog = shaders.getProgramID();
    } catch(Exception e) {
      e.printStackTrace();
    }

    uniform_tex = glGetUniformLocation(prog, "Texture");
    uniform_proj = glGetUniformLocation(prog, "ProjMtx");
    int attrib_pos = glGetAttribLocation(prog, "Transform");
    int attrib_uv = glGetAttribLocation(prog, "TexCoord");
    int attrib_col = glGetAttribLocation(prog, "Color");

    {
      // buffer setup
      vbo = glGenBuffers();
      ebo = glGenBuffers();
      vao = glGenVertexArrays();

      glBindVertexArray(vao);
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

      glEnableVertexAttribArray(attrib_pos);
      glEnableVertexAttribArray(attrib_uv);
      glEnableVertexAttribArray(attrib_col);

      glVertexAttribPointer(attrib_pos, 2, GL_FLOAT, false, NkDrawVertex.SIZEOF, NkDrawVertex.POS);
      glVertexAttribPointer(attrib_uv, 2, GL_FLOAT, false, NkDrawVertex.SIZEOF, NkDrawVertex.UV);
      glVertexAttribPointer(attrib_col, 4, GL_UNSIGNED_BYTE, true, NkDrawVertex.SIZEOF, NkDrawVertex.COL);
    }

    {
      // null texture setup
      int nullTexID = glGenTextures();

      null_texture.texture().id(nullTexID);
      null_texture.uv().set(0.5f, 0.5f);

      glBindTexture(GL_TEXTURE_2D, nullTexID);
      try(MemoryStack stack = stackPush()) {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, 1, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, stack.ints(0xFFFFFFFF));
      }
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    int BITMAP_W = 1024;
    int BITMAP_H = 1024;

    int FONT_HEIGHT = 18;
    int fontTexID = glGenTextures();

    STBTTFontinfo fontInfo = STBTTFontinfo.create();
    STBTTPackedchar.Buffer cdata = STBTTPackedchar.create(95);

    float scale;
    float descent;

    try(MemoryStack stack = stackPush()) {
      stbtt_InitFont(fontInfo, ttf);
      scale = stbtt_ScaleForPixelHeight(fontInfo, FONT_HEIGHT);

      IntBuffer d = stack.mallocInt(1);
      stbtt_GetFontVMetrics(fontInfo, null, d, null);
      descent = d.get(0) * scale;

      ByteBuffer bitmap = memAlloc(BITMAP_W * BITMAP_H);

      STBTTPackContext pc = STBTTPackContext.mallocStack(stack);
      stbtt_PackBegin(pc, bitmap, BITMAP_W, BITMAP_H, 0, 1, null);
      stbtt_PackSetOversampling(pc, 4, 4);
      stbtt_PackFontRange(pc, ttf, 0, FONT_HEIGHT, 32, cdata);
      stbtt_PackEnd(pc);

      // Convert R8 to RGBA8
      ByteBuffer texture = memAlloc(BITMAP_W * BITMAP_H * 4);
      for(int i = 0; i < bitmap.capacity(); i++)
      texture.putInt((bitmap.get(i) << 24) | 0x00FFFFFF);
      texture.flip();

      glBindTexture(GL_TEXTURE_2D, fontTexID);
      glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, BITMAP_W, BITMAP_H, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, texture);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

      memFree(texture);
      memFree(bitmap);
    }

    default_font
    .width((handle, h, text, len) -> {
      float text_width = 0;
      try(MemoryStack stack = stackPush()) {
        IntBuffer unicode = stack.mallocInt(1);

        int glyph_len = nnk_utf_decode(text, memAddress(unicode), len);
        int text_len = glyph_len;

        if(glyph_len == 0) {
          return 0;
        }

        IntBuffer advance = stack.mallocInt(1);
        while(text_len <= len && glyph_len != 0) {
          if(unicode.get(0) == NK_UTF_INVALID) {
            break;
          }

          /* query currently drawn glyph information */
          stbtt_GetCodepointHMetrics(fontInfo, unicode.get(0), advance, null);
          text_width += advance.get(0) * scale;

          /* offset next glyph */
          glyph_len = nnk_utf_decode(text + text_len, memAddress(unicode), len - text_len);
          text_len += glyph_len;
        }
      }
      return text_width;
    })
    .height(FONT_HEIGHT)
    .query((handle, font_height, glyph, codepoint, next_codepoint) -> {
      try(MemoryStack stack = stackPush()) {
        FloatBuffer x = stack.floats(0.0f);
        FloatBuffer y = stack.floats(0.0f);

        STBTTAlignedQuad q = STBTTAlignedQuad.malloc();
        IntBuffer advance = memAllocInt(1);

        stbtt_GetPackedQuad(cdata, BITMAP_W, BITMAP_H, codepoint - 32, x, y, q, false);
        stbtt_GetCodepointHMetrics(fontInfo, codepoint, advance, null);

        NkUserFontGlyph ufg = NkUserFontGlyph.create(glyph);

        ufg.width(q.x1() - q.x0());
        ufg.height(q.y1() - q.y0());
        ufg.offset().set(q.x0(), q.y0() + (FONT_HEIGHT + descent));
        ufg.xadvance(advance.get(0) * scale);
        ufg.uv(0).set(q.s0(), q.t0());
        ufg.uv(1).set(q.s1(), q.t1());

        memFree(advance);
        q.free();
      }
    })
    .texture().id(fontTexID);

    nk_style_set_font(ctx, default_font);
    nk_rgba_f(0.5f, 0.5f, 0.5f, 1.0f, gray);
    nk_rgba_f(0.3f, 0.3f, 0.3f, 1.0f, lightGray);
    nk_rgba_f(0.1f, 0.1f, 0.1f, 1.0f, darkGray);
    nk_rgba_f(0.0f, 0.0f, 0.0f, 0.0f, clear);

    bindings = new Bindings(world);
    bindings.mode = Bindings.Mode.UNIVERSAL;
    bindings.beforeInputDo = () -> {
      nk_input_begin(ctx);
      nk_input_motion(ctx, bindings.mousex, bindings.mousey);
    };
    bindings.afterInputDo = () -> {
      nk_input_end(ctx);
    };
    bindings.put(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_PRESS, () -> {
      nk_input_button(ctx, GLFW_MOUSE_BUTTON_LEFT, bindings.mousex, bindings.mousey, true);
    });
    bindings.put(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_RELEASE, () -> {
      nk_input_button(ctx, GLFW_MOUSE_BUTTON_LEFT, bindings.mousex, bindings.mousey, false);
    });
    bindings.put(GLFW_MOUSE_BUTTON_RIGHT, 0, GLFW_PRESS, () -> {
      nk_input_button(ctx, GLFW_MOUSE_BUTTON_RIGHT, bindings.mousex, bindings.mousey, true);
    });
    bindings.enable();
  }

  @Override
  public void process(int e) {
    UI ui = world.getMapper(UI.class).get(e);
    ui.draw(ctx);
  }

  @Override
  public void end() {
    nk_glfw3_new_frame();
    render(NK_ANTI_ALIASING_ON, MAX_VERTEX_BUFFER, MAX_ELEMENT_BUFFER);
    glfwSwapBuffers(win);
  }

  private void nk_glfw3_new_frame() {
    try(MemoryStack stack = stackPush()) {
      IntBuffer w = stack.mallocInt(1);
      IntBuffer h = stack.mallocInt(1);

      glfwGetWindowSize(win, w, h);
      width = w.get(0);
      height = h.get(0);

      glfwGetFramebufferSize(win, w, h);
      display_width = w.get(0);
      display_height = h.get(0);
    }

    /*
    if(ctx.input().mouse().grab()) {
      glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    } else if(ctx.input().mouse().grabbed()) {
      glfwSetCursorPos(win, ctx.input().mouse().prev().x(), ctx.input().mouse().prev().y());
    } else if(ctx.input().mouse().ungrab()) {
      glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }*/
  }

  private void render(int AA, int max_vertex_buffer, int max_element_buffer) {
    try(MemoryStack stack = stackPush()) {
      // setup global state
      glDisable(GL_CULL_FACE);
      glDisable(GL_DEPTH_TEST);
      glEnable(GL_SCISSOR_TEST);
      glActiveTexture(GL_TEXTURE0);

      // setup program
      shaders.bind();
      glUniform1i(uniform_tex, 0);
      glUniformMatrix4fv(uniform_proj, false, stack.floats(
      2.0f / width, 0.0f, 0.0f, 0.0f,
      0.0f, -2.0f / height, 0.0f, 0.0f,
      0.0f, 0.0f, -1.0f, 0.0f,
      -1.0f, 1.0f, 0.0f, 1.0f
      ));
      glViewport(0, 0, display_width, display_height);
    }

    {
      // convert from command queue into draw list and draw to screen

      // allocate vertex and element buffer
      glBindVertexArray(vao);
      glBindBuffer(GL_ARRAY_BUFFER, vbo);
      glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);

      glBufferData(GL_ARRAY_BUFFER, max_vertex_buffer, GL_STREAM_DRAW);
      glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_element_buffer, GL_STREAM_DRAW);

      // load draw vertices & elements directly into vertex + element buffer
      ByteBuffer vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY, max_vertex_buffer, null);
      ByteBuffer elements = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY, max_element_buffer, null);
      try(MemoryStack stack = stackPush()) {
        // fill converting configuration
        NkConvertConfig config = NkConvertConfig.callocStack(stack)
        .global_alpha(1.0f)
        .shape_AA(AA)
        .line_AA(AA)
        .circle_segment_count(22)
        .curve_segment_count(22)
        .arc_segment_count(22)
        .null_texture(null_texture);

        // setup buffers to load vertices and elements
        NkBuffer vbuf = NkBuffer.mallocStack(stack);
        NkBuffer ebuf = NkBuffer.mallocStack(stack);

        nk_buffer_init_fixed(vbuf, vertices/*, max_vertex_buffer*/);
        nk_buffer_init_fixed(ebuf, elements/*, max_element_buffer*/);
        nk_convert(ctx, cmds, vbuf, ebuf, config);
      }
      glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);
      glUnmapBuffer(GL_ARRAY_BUFFER);

      // iterate over and execute each draw command
      float fb_scale_x = (float)display_width / (float)width;
      float fb_scale_y = (float)display_height / (float)height;

      long offset = NULL;
      for(NkDrawCommand cmd = nk__draw_begin(ctx, cmds); cmd != null; cmd = nk__draw_next(cmd, cmds, ctx)) {
        if(cmd.elem_count() == 0) continue;
        glBindTexture(GL_TEXTURE_2D, cmd.texture().id());
        glScissor(
        (int)(cmd.clip_rect().x() * fb_scale_x),
        (int)((height - (int)(cmd.clip_rect().y() + cmd.clip_rect().h())) * fb_scale_y),
        (int)(cmd.clip_rect().w() * fb_scale_x),
        (int)(cmd.clip_rect().h() * fb_scale_y)
        );
        glDrawElements(GL_TRIANGLES, cmd.elem_count(), GL_UNSIGNED_SHORT, offset);
        offset += cmd.elem_count() * 2;
      }
      nk_clear(ctx);
    }

    // default OpenGL state
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_SCISSOR_TEST);
  }

  public void drawStatsGrid(ShardGrid shards, NkCommandBuffer canvas, NkRect bounds, boolean drawFog) {
    int gridLength = (int)Math.min(bounds.w(), bounds.h());
    int gridSize = gridLength / shards.size;
    int x = Math.round(bounds.x());
    int y = Math.round(bounds.y());

    /* draw grid nodes */
    for(int nx = 0; nx < shards.width(); nx++) {
      for(int ny = 0; ny < shards.height(); ny++) {
        drawNode(canvas, shards, nx, ny, x, y, gridSize, drawFog);
      }
    }

    /* draw vertical grid dividers/boundaries */
    for(int nx = 1; nx < shards.width(); nx++) {
      int gx = x + (nx * gridSize);
      for(int ny = 0; ny < shards.height(); ny++) {
        int gy = y + (ny * gridSize);
        if(shouldDrawThinBoundary(shards, nx, ny, nx - 1, ny, drawFog)) {
          nk_stroke_line(canvas, gx, gy, gx, gy + gridSize, 1, lightGray);
        } else {
          nk_stroke_line(canvas, gx, gy, gx, gy + gridSize, 4, lightGray);
        }
      }
    }

    /* draw horizontal grid dividers/boundaries */
    for(int nx = 0; nx < shards.width(); nx++) {
      int gx = x + (nx * gridSize);
      for(int ny = 1; ny < shards.height(); ny++) {
        int gy = y + (ny * gridSize);
        if(shouldDrawThinBoundary(shards, nx, ny, nx, ny - 1, drawFog)) {
          nk_stroke_line(canvas, gx, gy, gx + gridSize, gy, 1, lightGray);
        } else {
          nk_stroke_line(canvas, gx, gy, gx + gridSize, gy, 4, lightGray);
        }
      }
    }

    /* draw grid outline */
    nk_stroke_line(canvas, x, y, x + bounds.w(), y, 4, lightGray);
    nk_stroke_line(canvas, x, y, x, y + bounds.h(), 4, lightGray);
    nk_stroke_line(canvas, x + bounds.w(), y, x + bounds.w(), y + bounds.h(),
    4, lightGray);
    nk_stroke_line(canvas, x, y + bounds.h(), x + bounds.w(), y + bounds.h(),
    4, lightGray);
  }

  public void drawShardInRect(NkCommandBuffer canvas, Shard shard, NkRect rect) {
    Vector4i dims = getDimensions(shard);
    int lenx = dims.z - dims.x + 1;
    int leny = dims.w - dims.y + 1;
    int gridx = (int)(rect.w() / lenx);
    int gridy = (int)(rect.h() / leny);
    int grid = Math.min(gridx, gridy) - 4;
    int centerx = (int)rect.x() + (int)(rect.w() / 2) - (Math.abs(dims.z) * grid / 2) + (Math.abs(dims.x) * grid / 2);
    int centery = (int)rect.y() + (int)(rect.h() / 2) - (Math.abs(dims.w) * grid / 2) + (Math.abs(dims.y) * grid / 2);
    NkColor color = NkColor.create();
    for(Node n : shard.nodes) {
      NkRect node = NkRect.create();
      nk_rect(centerx + (n.x * grid) - (grid / 2),
      centery + (n.y * grid) - (grid / 2),
      grid, grid, node);
      nk_rgba_f(shard.color.x, shard.color.y, shard.color.z, shard.color.w, color);
      nk_fill_rect(canvas, node, 1, color);
    }
  }

  public Vector4i getDimensions(Shard shard) {
    int maxx = Integer.MIN_VALUE, maxy = Integer.MIN_VALUE;
    int minx = Integer.MAX_VALUE, miny = Integer.MAX_VALUE;
    for(Node n : shard.nodes) {
      maxx = Math.max(maxx, n.x);
      minx = Math.min(minx, n.x);
      maxy = Math.max(maxy, n.y);
      miny = Math.min(miny, n.y);
    }
    return new Vector4i(minx, miny, maxx, maxy);
  }

  public void drawShard(NkCommandBuffer canvas, Shard shard, int x, int y, int gridSize) {
    NkColor color = NkColor.create();
    for(Node n : shard.nodes) {
      NkRect rect = NkRect.create();
      nk_rect(x + (n.x * gridSize) - (gridSize / 2),
      y + (n.y * gridSize) - (gridSize / 2),
      gridSize, gridSize, rect);
      nk_rgba_f(shard.color.x, shard.color.y, shard.color.z, 0.3f, color);
      nk_fill_rect(canvas, rect, 1, color);
    }
  }

  public void drawNode(NkCommandBuffer canvas, ShardGrid shards, int x, int y, int gx, int gy, int gridSize, boolean drawFog) {
    NkRect rect = NkRect.create();
    NkRect text = NkRect.create();
    nk_rect(x * gridSize + gx, y * gridSize + gy, gridSize, gridSize, rect);
    nk_rect(rect.x() + (gridSize / 2), rect.y() + (gridSize / 2), gridSize, gridSize, text);
    if(drawFog && shards.fog.get(x, y)) {
      nk_fill_rect(canvas, rect, 0.9f, darkGray);
      nk_draw_text(canvas, text, "?", default_font, clear, lightGray);
    } else if(!shards.grid.isEmpty(x, y)) {
      Node node = shards.grid.get(x, y);
      Shard shard = node.shard;
      NkColor color = NkColor.create();
      if(node.enabled) {
        nk_rgba_f(shard.color.x, shard.color.y, shard.color.z, shard.color.w, color);
        nk_fill_rect(canvas, rect, 0.9f, color);
      } else {
        nk_fill_rect(canvas, rect, 0.9f, gray);
      }
    }
  }

  public boolean shouldDrawThinBoundary(ShardGrid shards, int x1, int y1, int x2, int y2, boolean drawFog) {
    return (shards.isSameShard(x1, y1, x2, y2) && (!shards.isFogBoundary(x1, y1, x2, y2) || !drawFog)) ||
    shards.betweenEmptiness(x1, y1, x2, y2) ||
    (shards.betweenFog(x1, y1, x2, y2) && drawFog);
  }
}
