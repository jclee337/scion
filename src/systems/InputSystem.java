import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;
import org.lwjgl.BufferUtils;

import java.nio.DoubleBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.lwjgl.glfw.GLFW.*;

public class InputSystem extends BaseEntitySystem {

  private WindowSystem window;
  private long windowID;
  private LinkedList<Integer> contexts = new LinkedList<>();
  private LinkedList<Integer> universals = new LinkedList<>();
  private ComponentMapper<Context> mContext;

  public InputSystem() {
    super(Aspect.all(Context.class));
  }

  @Override
  public void inserted(int e) {
    Bindings b = mContext.get(e).bindings;
    if(b.mode == Bindings.Mode.UNIVERSAL) {
      universals.push(e);
    } else {
      contexts.push(e);
    }
  }

  @Override
  public void removed(int e) {
    contexts.remove((Integer)e);
    universals.remove((Integer)e);
  }

  @Override
  public void initialize() {
    windowID = window.getWindow();
    glfwSetKeyCallback(window.getWindow(), (window, key, scancode, action, mods) -> {
      for(Integer e : contexts) {
        Bindings b = mContext.get(e).bindings;
        if(b.has(key, mods, action)) {
          b.get(key, mods, action).run();
          break;
        } else if(b.mode == Bindings.Mode.CONSUMING) {
          break;
        }
      }

      for(Integer e : universals) {
        Bindings b = mContext.get(e).bindings;
        if(b.has(key, mods, action)) {
          b.get(key, mods, action).run();
        }
      }
    });
    glfwSetMouseButtonCallback(window.getWindow(), (window, button, action, mods) -> {
      for(Integer e : contexts) {
        Bindings b = mContext.get(e).bindings;
        if(b.has(button, mods, action)) {
          b.get(button, mods, action).run();
          break;
        } else if(b.mode == Bindings.Mode.CONSUMING) {
          break;
        }
      }

      for(Integer e : universals) {
        Bindings b = mContext.get(e).bindings;
        if(b.has(button, mods, action)) {
          b.get(button, mods, action).run();
        }
      }
    });
    glfwSetCursorPosCallback(window.getWindow(), (window, x, y) -> {
      updateMouse(contexts, (int)x, (int)y);
      updateMouse(universals, (int)x, (int)y);
    });
  }

  @Override
  public void processSystem() {
    pre(contexts);
    pre(universals);
    glfwPollEvents();
    post(contexts);
    post(universals);
  }

  private void updateMouse(LinkedList<Integer> list, int x, int y) {
    for(Integer e : list) {
      Bindings b = mContext.get(e).bindings;
      b.mousex = (int)x;
      b.mousey = (int)y;
    }
  }

  private void pre(LinkedList<Integer> list) {
    for(Integer e : list) {
      Bindings b = mContext.get(e).bindings;
      b.beforeInputDo.run();
    }
  }

  private void post(LinkedList<Integer> list) {
    for(Integer e : list) {
      Bindings b = mContext.get(e).bindings;
      b.afterInputDo.run();
    }
  }
}
