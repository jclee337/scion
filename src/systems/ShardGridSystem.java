import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import org.joml.Vector2i;
import org.joml.Vector4f;
import org.joml.Vector4i;
import org.lwjgl.BufferUtils;
import org.lwjgl.nuklear.*;
import org.lwjgl.nuklear.NkCommandBuffer;
import org.lwjgl.nuklear.NkInput;
import org.lwjgl.system.MemoryStack;

import java.nio.ByteBuffer;
import java.nio.DoubleBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.ArrayList;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nuklear.Nuklear.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL30.*;
import static org.lwjgl.stb.STBImage.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class ShardGridSystem extends BaseEntitySystem {

  private GUIRenderSystem gui;
  private Vector2i mouse;
  private NkImage image;
  private Integer current;
  private ShardGrid shards;
  private Inventory invent;
  private Shard selected;
  private Vector2i prev;
  private int gridSize, padding;
  private ComponentMapper<ShardGrid> mShards;
  private ComponentMapper<Inventory> mInvent;
  private int hidden = NK_WINDOW_HIDDEN;
  private NkPanel layout;
  private NkPanel group;
  private NkRect bounds;
  private NkRect gridBounds, inventBounds;
  private NkColor color;
  private NkInput input;

  public ShardGridSystem() {
    super(Aspect.all(ShardGrid.class, Selected.class));
    gridSize = 60;
    padding = 30;
    mouse = new Vector2i(0, 0);
  }

  @Override
  public void initialize() {
    // preparing image for use in gui
    // not used, but keeping this around to remember how to do it
    IntBuffer x = BufferUtils.createIntBuffer(1);
    IntBuffer y = BufferUtils.createIntBuffer(1);
    IntBuffer comp = BufferUtils.createIntBuffer(1);
    ByteBuffer data = stbi_load("images/white.png", x, y, comp, 4);
    int tex = glGenTextures();
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, x.get(0), y.get(0), 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);
    image = NkImage.create();
    nk_image_id(tex, image);

    layout = NkPanel.create();
    group = NkPanel.create();
    bounds = NkRect.create();
    gridBounds = NkRect.create();
    inventBounds = NkRect.create();
    color = NkColor.create();
    input = gui.ctx.input();
  }

  @Override
  public boolean checkProcessing() {
    return current != null && shards.visible;
  }

  @Override
  public void inserted(int e) {
    hidden = 0;
    current = e;
    shards = mShards.get(e);
    if(mInvent.has(e)) {
      invent = mInvent.get(e);
    }
  }

  @Override
  public void begin() {
    nk_begin(gui.ctx, layout, "Shard Grid",
    nk_rect(300, 100, 700, 500, bounds),
    NK_WINDOW_BORDER | NK_WINDOW_NO_SCROLLBAR | NK_WINDOW_TITLE | hidden);
  }

  @Override
  public void processSystem() {
    NkCommandBuffer canvas = nk_window_get_canvas(gui.ctx);
    nk_window_get_content_region(gui.ctx, bounds);

    /* calculate window and submenu dimensions */
    nk_window_get_content_region(gui.ctx, bounds);
    int limiter = (int)Math.min(bounds.w() * 2 / 3, bounds.h());
    int gridLength = (limiter - (padding * 2));
    gridSize = gridLength / shards.width();
    nk_rect(bounds.x() + (bounds.w() / 3) - (gridLength / 2),
      bounds.y() + (bounds.h() / 2) - (gridLength / 2),
      gridLength, gridLength, gridBounds);
    nk_rect((bounds.w() * 2 / 3), -5,
      bounds.w() / 3, bounds.h(), inventBounds);

    /* calculate grid cell position of mouse */
    int x = (int)Math.floor((mouse.x - bounds.x() - padding) / gridSize);
    int y = (int)Math.floor((mouse.y - bounds.y() - padding) / gridSize);

    /* process input */
    if(nk_input_is_key_pressed(input, GLFW_KEY_ESCAPE)) {

    }

    if(nk_input_is_mouse_pressed(input, GLFW_MOUSE_BUTTON_LEFT) && selected != null) {
      if(shards.canPlace(selected, x, y)) {
        shards.placeShard(selected, x, y);
      } else if(prev == null) {
        invent.shards.add(selected);
      } else {
        shards.placeShard(selected, prev.x, prev.y);
      }
      selected = null;
    } else if(nk_input_is_mouse_pressed(input, GLFW_MOUSE_BUTTON_LEFT) && selected == null) {
      if(shards.inBounds(x, y) && !shards.grid.isEmpty(x, y)) {
        Node n = shards.grid.get(x, y);
        prev = new Vector2i(x - n.x, y - n.y);
        selected = shards.removeShard(x, y);
      }
    }
    if(nk_input_is_mouse_pressed(input, GLFW_MOUSE_BUTTON_RIGHT) && selected != null) {
      invent.shards.add(selected);
      selected = null;
    }

    if(shards.inBounds(x, y) && !shards.grid.isEmpty(x, y)) {
      drawStats(canvas, shards.grid.get(x, y).shard);
    }

    /* draw the grid */
    gui.drawStatsGrid(shards, canvas, gridBounds, true);

    /* draw inventory submenu */
    Shard hover = null;
    if(invent != null) {
      nk_layout_space_begin(gui.ctx, NK_STATIC, gridBounds.h(), 1);
      nk_layout_space_push(gui.ctx, inventBounds);
      if(nk_group_begin(gui.ctx, group, "Inventory", NK_WINDOW_BORDER)) {
        for(int i = 0; i < invent.shards.size(); i++) {
          if(i % 2 == 0) {
            nk_layout_row_dynamic(gui.ctx, 100, 2);
          }
          Shard shard = invent.shards.get(i);
          NkRect rect = NkRect.create();
          nk_widget_bounds(gui.ctx, rect);
          if(nk_input_is_mouse_hovering_rect(input, rect)) {
            hover = shard;
          }
          if(nk_button_text(gui.ctx, "") && selected == null) {
            selected = invent.shards.remove(i);
            prev = null;
          }
          gui.drawShardInRect(canvas, shard, rect);
        }
        nk_group_end(gui.ctx);
      }
      nk_layout_space_end(gui.ctx);
    }

    if(hover != null) {
      drawStats(canvas, hover);
    }

    // draw selected shard
    if(selected != null) {
      gui.drawShard(canvas, selected, mouse.x, mouse.y, gridSize);
    }
  }

  @Override
  public void end() {
    nk_end(gui.ctx);
  }

  @Override
  public void removed(int e) {
    hidden = NK_WINDOW_HIDDEN;
    current = null;
    shards.visible = false;
    shards = null;
    invent = null;
  }

  public void drawStats(NkCommandBuffer canvas, Shard shard) {
    try(MemoryStack stack = stackPush()) {
      NkRect pos = NkRect.mallocStack(stack);
      nk_rect(gridBounds.x() + padding, gridBounds.y(), 100, 400, pos);
      NkColor bg = NkColor.mallocStack(stack);
      NkColor fg = NkColor.mallocStack(stack);
      nk_rgba_f(0, 0, 0, 0, bg);
      nk_rgba_f(1, 1, 1, 1, fg);
      String text = "| ";
      if(shard.stats.precision > 0) {
        text += ("Precision +" + shard.stats.precision + " | ");
      }
      if(shard.stats.strength > 0) {
        text += "Strength +" + shard.stats.strength + " | ";
      }
      if(shard.stats.speed > 0) {
        text += "Speed +" + shard.stats.speed + " | ";
      }
      nk_draw_text(canvas, pos, text, gui.default_font, bg, fg);
    }
  }
}
