

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.systems.IteratingSystem;

public class TransitionSystem extends IteratingSystem {

  private ComponentMapper<Transform> mTform;
  private ComponentMapper<Transition> mTrans;

  public TransitionSystem() {
    super(Aspect.all(Transform.class, Transition.class));
  }

  @Override
  public void inserted(int e) {
    Transform transform = mTform.get(e);
    Transition transition = mTrans.get(e);
    transition.origin = new Transform(transform);
    transition.accumulated = 0;
  }

  @Override
  public void process(int e) {
    Transform transform = mTform.get(e);
    Transition transition = mTrans.get(e);
    Transform origin = transition.origin;
    Transform goal = transition.goal;
    boolean finished = false;

    transition.accumulated += world.getDelta();
    float t;
    if(transition.accumulated > transition.duration) {
      t = 1;
      finished = true;
    } else {
      t = transition.accumulated / transition.duration;
    }

    transform.position.x = lerp(origin.position.x, goal.position.x, t);
    transform.position.y = lerp(origin.position.y, goal.position.y, t);
    transform.position.z = lerp(origin.position.z, goal.position.z, t);
    transform.scale = lerp(origin.scale, goal.scale, t);
    transform.rotation.x = lerp(origin.rotation.x, goal.rotation.x, t);
    transform.rotation.y = lerp(origin.rotation.y, goal.rotation.y, t);
    transform.rotation.z = lerp(origin.rotation.z, goal.rotation.y, t);

    if(finished) {
      mTrans.remove(e);
    }
  }

  public float lerp(float o, float g, float t) {
    t = (float)(1 - Math.cos(t * Math.PI)) / 2;
    return ((1 - t) * o) + (t * g);
  }
}
