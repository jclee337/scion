import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import org.joml.Vector3f;

import java.util.ArrayList;
import java.util.PriorityQueue;

import static org.lwjgl.opengl.GL11.*;

public class TurnSystem extends BaseEntitySystem {

  private ArrayList<Entry> entities = new ArrayList<>();

  private int current = 0;
  private int maxSpeed = Integer.MIN_VALUE;
  private double speedScale = 2;
  private int indicator;
  private float hover;
  private ComponentMapper<ShardGrid> mShards;
  private ComponentMapper<Transform> mTform;

  public TurnSystem() {
    super(Aspect.all(ShardGrid.class, Coords.class));
  }

  @Override
  public void initialize() {
    initIndicator();
    /*
    Comparator<Coords> comp = (Integer e1, Integer e2) -> {
      return mStats.get(e1).speed - mStats.get(e2).speed;
    };
    entities = new PriorityQueue<Integer>(comp);
    */
    Utils.listener(world, Aspect.all(ShardGrid.class, Controllable.class),
    inserted -> {},
    removed -> {
      world.getMapper(Controllable.class).remove(current);
      System.out.println("Next turn starting.");
      current = next();
      world.getMapper(Controllable.class).create(current);
    });
  }

  @Override
  public void inserted(int e) {
    int speed = mShards.get(e).total.speed;
    System.out.println("Inserting " + e + " into turn system.");
    if(speed > maxSpeed) {
      // rescale everything
      double ratio = ((double)(speed) / maxSpeed) * (2 / speedScale);
      for(int i = 0; i < entities.size(); i++) {
        entities.get(i).time *= ratio;
      }
      maxSpeed = speed;
    }
    double time = ((double)(maxSpeed) / speed) * (2 / speedScale);
    Entry entry = new Entry(e, time);
    if(entities.isEmpty()) {
      //System.out.println("Queue is empty. " + e + " added to queue.");
      entities.add(entry);
      current = e;
      world.getMapper(Controllable.class).create(e);
    } else {
      for(int i = 0; i < entities.size(); i++) {
        if(entities.get(i).time >= time) {
          entities.add(i, entry);
          //System.out.println(e + " added to queue.");
          break;
        } else if(i == entities.size() - 1) {
          entities.add(entry);
          break;
          //System.out.println(e + " added to end of queue.");
        }
      }
    }
    /*
    int index = binary(e);
    System.out.println("inserted at: " + index);
    if(index >= entities.size()) {
      entities.add(e);
    } else {
      entities.add(index, e);
    }
    if(index < current) {
      current += 1;
    }
    if(entities.size() == 1) {
      world.getMapper(Controllable.class).create(e);
    }
    */
    //entities.add(e);
  }

  @Override
  public void removed(int e) {
    for(int i = 0; i < entities.size(); i++) {
      if(entities.get(i).entity == e) {
        entities.remove(i);
        break;
      }
    }
    //entities.remove(binary(e));
    //entities.remove(e);
  }

  @Override
  public void processSystem() {
    Transform selPos = mTform.get(current);
    Transform indPos = mTform.get(indicator);
    indPos.position.set(selPos.position);
    indPos.position.add(0, 0, (float)Math.sin(hover) * 0.3f + 1.8f);
    hover += 0.1;
    if(hover >= 2*Math.PI) {
      hover %= 2*Math.PI;
    }
  }

  /**
  * if the specified entity is in the list, this returns the index of the entity
  * otherwise, this returns the last possible index it could go if inserted
  */
  /*
  private int binary(int e) {
    int speed = mShards.get(e).total.speed;
    int lo = 0;
    int hi = entities.size() - 1;
    int index = (lo + hi) / 2;
    for(; lo <= hi; index = (lo + hi) / 2) {
      if(mStats.get(entities.get(index)).speed > speed) {
        lo = index + 1;
      } else if(mStats.get(entities.get(index)).speed < speed) {
        hi = index - 1;
      } else {
        if(entities.get(index) != e) {
          int first = index - 1;
          int last = index + 1;
          while(first >= 0 && mStats.get(entities.get(first)).speed == speed) {
            if(entities.get(first) == e) {
              return first;
            }
            first--;
          }
          while(last < entities.size() && mStats.get(entities.get(last)).speed == speed) {
            if(entities.get(last) == e) {
              return last;
            }
            last++;
          }
          index = last;
        }
        return index;
      }
    }
    return lo;
  }
  */

  private int next() {
    /*
    if(current >= entities.size() - 1) {
      return 0;
    }
    return current + 1;
    */
    Entry entry = entities.remove(0);
    int speed = mShards.get(entry.entity).total.speed;
    if(entry.time > 0) {
      for(int i = 0; i < entities.size(); i++) {
        entities.get(i).time -= entry.time;
      }
      entry.time = (((double)(maxSpeed) / speed) * (2.0 / speedScale));
    } else {
      entry.time = (((double)(maxSpeed) / speed) * (2.0 / speedScale));
    }
    if(entities.isEmpty()) {
      entities.add(entry);
      System.out.println("Queue is empty. " + entry.entity + " added to queue.");
    } else {
      for(int i = 0; i < entities.size(); i++) {
        if(entities.get(i).time >= entry.time) {
          entities.add(i, entry);
          System.out.println(entry.entity + " added to queue.");
          break;
        } else if(i == entities.size() - 1) {
          entities.add(entry);
          System.out.println(entry.entity + " added to end of queue.");
          break;
        }
      }
    }
    for(int i = 0; i < entities.size(); i++) {
      System.out.println("Entity " + entities.get(i).entity + " has turn " + i + " with time left " + entities.get(i).time);
    }
    current = entry.entity;
    System.out.println("Num turn takers: " + entities.size());
    return current;
  }

  private class Entry {
    public int entity;
    public double time;

    public Entry(int entity, double time) {
      this.entity = entity;
      this.time = time;
    }
  }

  private void initIndicator() {
    indicator = world.create();
    Transform pos = mTform.create(indicator);
    pos.position = new Vector3f(0, 0, 0);
    pos.rotation = new Vector3f(0, 0, 0);
    pos.scale = 1;
    Mesh mesh = world.getMapper(Mesh.class).create(indicator);
    mesh.vertices = new float[] {
      -0.12f,  0.12f, 0.0f,
      0.12f, 0.12f, 0.0f,
      0.12f, -0.12f, 0.0f,
      -0.12f,  -0.12f, 0.0f,
      0.0f, 0.0f, 0.4f,
      0.0f, 0.0f, -0.4f
    };
    mesh.colors = new float[] {
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      0.5f, 0.5f, 0.5f, 1.0f,
      0.5f, 0.5f, 0.5f, 1.0f
    };
    mesh.indices = new int[] {
      0, 1, 3, // 1/2 bottom face
      3, 1, 2, // 1/2 bottom face
      0, 1, 4,
      1, 2, 4,
      2, 3, 4,
      3, 0, 4,
      0, 1, 5,
      1, 2, 5,
      2, 3, 5,
      3, 0, 5
    };
    mesh.mode = GL_TRIANGLES;
  }
}
