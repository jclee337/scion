import com.artemis.BaseSystem;
import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.system.MemoryUtil.*;

public class WindowSystem extends BaseSystem {

  private long window;
  private int width, height;

  public WindowSystem() {

    GLFWErrorCallback.createPrint(System.err).set();
    if(!glfwInit())
      throw new IllegalStateException("Unable to initialize GLFW.");

    glfwDefaultWindowHints();
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_SAMPLES, 8);

    width = 1280;
    height = 720;
    window = glfwCreateWindow(width, height, "LWJGL Test", NULL, NULL);

    if(window == NULL)
      throw new RuntimeException("Failed to create window");

    GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

    glfwSetWindowPos(window,
    (vidmode.width() - width) / 2,
    (vidmode.height() - height) / 2);

    glfwMakeContextCurrent(window);
    //glfwSwapInterval(1);
    glfwShowWindow(window);

    GL.createCapabilities();
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    //glBlendFunc(GL_ZERO, GL_SRC_COLOR);
    //glBlendFunc(GL_ONE, GL_ONE);
  }

  @Override
  public void initialize() {

  }

  @Override
  public void processSystem() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  }

  public long getWindow() {
    return window;
  }

  public int getWidth() {
    return width;
  }

  public int getHeight() {
    return height;
  }

  public boolean isKeyPressed(int keyCode) {
    return glfwGetKey(window, keyCode) == GLFW_PRESS;
  }
}
