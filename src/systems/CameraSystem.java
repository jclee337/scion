import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IteratingSystem;

import static org.lwjgl.glfw.GLFW.*;

public class CameraSystem extends IteratingSystem {
  private ComponentMapper<Transform> mTform;
  private float rotation = (float)(-Math.PI / 4.0f);
  private final float turn = (float)(Math.PI / 4.0);
  private float radius = 130.0f, theta = 1.0f, phi = (float)(-Math.PI / 4.0f);
  private Bindings bindings;

  public CameraSystem() {
    super(Aspect.all(Camera.class, Transform.class));
  }

  @Override
  public void initialize() {
    bindings = new Bindings(world);
    bindings.mode = Bindings.Mode.DEFERRING;
    bindings.put(GLFW_KEY_A, 0, GLFW_PRESS, () -> {
      rotation -= turn;
    });
    bindings.put(GLFW_KEY_D, 0, GLFW_PRESS, () -> {
      rotation += turn;
    });
    bindings.enable();
  }

  @Override
  public void process(int e) {
    Transform pos = mTform.get(e);
    phi -= (phi - rotation) * 0.1f;
    pos.position.set((float)(radius * Math.sin(theta) * Math.cos(phi)),
    (float)(radius * Math.sin(theta) * Math.sin(phi)),
    (float)(radius * Math.cos(theta)));
 }
}
