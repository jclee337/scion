import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.IteratingSystem;
import org.joml.Vector2i;

import static org.lwjgl.opengl.GL11.*;

public class GridTurnSystem extends IteratingSystem {

  private ComponentMapper<Transform> mTform;
  private ComponentMapper<Coords> mGridSpace;
  private Grid<Integer> selectors = new Grid<>();
  private int turn;
  private Vector2i turnPos = new Vector2i();

  public GridTurnSystem() {
    super(Aspect.all(Coords.class, Stats.class, Controllable.class));
  }

  @Override
  public void initialize() {
    initSelectors();
  }

  @Override
  public void inserted(int e) {
    int size = 15;
    Coords pos = mGridSpace.get(e);
    turnPos.set(pos.x(), pos.y());
    for(int x = pos.x() - 1; x <= pos.x() + 1; x++) {
      for(int y = pos.y() - 1; y <= pos.y() + 1; y++) {
        if(selectors.withinArea(x, y, -size / 2, -size / 2, size / 2, size / 2)) {
          world.getMapper(Invisible.class).remove(selectors.get(x, y));
        }
      }
    }
  }

  @Override
  public void process(int e) {

  }

  @Override
  public void removed(int e) {
    int size = 15;
    for(int x = turnPos.x - 1; x <= turnPos.x + 1; x++) {
      for(int y = turnPos.y - 1; y <= turnPos.y + 1; y++) {
        if(selectors.withinArea(x, y, -size / 2, -size / 2, size / 2, size / 2)) {
          world.getMapper(Invisible.class).create(selectors.get(x, y));
        }
      }
    }
  }

  private void initSelectors() {
    float cell = 1.5f;
    int size = 15;
    for(int x = -size / 2; x <= size / 2; x++) {
      for(int y = -size / 2; y <= size / 2; y++) {
        int e = world.create();
        Coords grid = world.getMapper(Coords.class).create(e);
        grid.move(x, y);
        Transform trans = world.getMapper(Transform.class).create(e);
        trans.position.x = cell * x;
        trans.position.y = cell * y;
        world.getMapper(Invisible.class).create(e);
        Mesh mesh = world.getMapper(Mesh.class).create(e);
        mesh.vertices = new float[] {
          -1.5f / 4, 1.5f / 4, 0,
          1.5f / 4, 1.5f / 4, 0,
          1.5f / 4, -1.5f / 4, 0,
          -1.5f / 4, -1.5f / 4, 0
        };
        mesh.colors = new float[] {
          0.5f, 0.7f, 0.5f, 1.0f,
          0.5f, 0.7f, 0.5f, 1.0f,
          0.5f, 0.7f, 0.5f, 1.0f,
          0.5f, 0.7f, 0.5f, 1.0f
        };
        mesh.indices = new int[] {
          0, 1, 3,
          3, 1, 2
        };
        mesh.mode = GL_TRIANGLES;
        selectors.put(x, y, e);
      }
    }
  }
}
