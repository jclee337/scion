import com.artemis.Aspect;
import com.artemis.BaseSystem;
import com.artemis.systems.IteratingSystem;
import org.lwjgl.nuklear.*;
import org.lwjgl.system.MemoryStack;

import static org.lwjgl.nuklear.Nuklear.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;

public class AllyTurnSystem extends IteratingSystem {

  private WindowSystem window;
  private GUIRenderSystem gui;
  private int hidden = NK_WINDOW_HIDDEN;
  private NkPanel layout;
  private NkRect bounds;

  public AllyTurnSystem() {
    super(Aspect.all(Ally.class, Stats.class, Coords.class, Controllable.class).exclude(Player.class));
  }

  public void initialize() {
    layout = NkPanel.create();
    bounds = NkRect.create();
  }

  @Override
  public void inserted(int e) {
    hidden = 0; // set window to not hidden
  }

  @Override
  public void begin() {
    nk_begin(gui.ctx, layout, "Unit Actions",
    nk_rect(window.getWidth() - 250, window.getHeight() - 250, 250, 250, bounds),
    NK_WINDOW_TITLE | NK_WINDOW_BORDER | hidden);
  }

  @Override
  public void process(int e) {
    nk_layout_row_dynamic(gui.ctx, 40, 2);
    nk_spacing(gui.ctx, 1);
    if(nk_button_text(gui.ctx, "End Turn")) {
      world.getMapper(Controllable.class).remove(e);
    }
  }

  @Override
  public void end() {
    nk_end(gui.ctx);
  }

  @Override
  public void removed(int e) {
    hidden = NK_WINDOW_HIDDEN;
  }
}
