import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySubscription;
import com.artemis.World;
import com.artemis.utils.IntBag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class GridSystem extends BaseEntitySystem {

  protected Grid<Integer> grid = new Grid<Integer>();
  protected HashMap<Integer, Coords> positions = new HashMap<>();
  private GridHelper helper;
  private ComponentMapper<Coords> mGrid;
  protected static int size = 15;
  protected static float cell = 1.5f;
  public static final Grid<Coords> tiles = new Grid<>();

  static {
    for(int x = (-size / 2) - 1; x <= (size / 2) + 1; x++) {
      for(int y = (-size / 2) - 1; y <= (size / 2) + 1; y++) {
        tiles.put(x, y, new Coords(x, y));
      }
    }
  }

  public GridSystem(Aspect.Builder primeAspect, Aspect.Builder gridAspect) {
    super(primeAspect);
    // the apsect of the entities in the grid must have a Coords component
    helper = new GridHelper(gridAspect.all(Coords.class));
  }

  @Override
  public void initialize() {
    helper.initialize(world);
  }

  @Override
  public void processSystem() {
    helper.processSystem();
    IntBag actives = subscription.getEntities();
    int[] ids = actives.getData();
    for(int i = 0; i < actives.size(); i++) {
      if(ids[i] != 0) {
        process(ids[i]);
      }
    }
  }

  protected abstract void process(int e);

  protected void insertedIntoGrid(int e) {

  }

  protected void removedFromGrid(int e) {

  }

  protected abstract void moved(int e);

  /*
  protected void move(int e, int x, int y) {
    Coords current = mGrid.get(e);
    current.x = x;
    current.y = y;
    Coords latest = positions.get(e);
    if(latest == null) {
      grid.put(current.x, current.y, e);
      positions.put(e, tiles.get(current.x, current.y));
      moved(e);
    } else if((current.x != latest.x || current.y != latest.y)) {
      grid.remove(latest.x, latest.y);
      grid.put(current.x, current.y, e);
      positions.put(e, tiles.get(current.x, current.y));
      moved(e);
    }
  }
  */

  protected List<Integer> getNeighborsInRange(int x, int y, int range) {
    List<Integer> neighbors = new ArrayList<Integer>();
    for(int sx = x - range; sx <= x + range; sx++) {
      for(int sy = y - range; sy <= y + range; sy++) {
        if((sx != x || sy != y) && !grid.isEmpty(sx, sy)) {
          neighbors.add(grid.get(sx, sy));
        }
      }
    }
    return neighbors;
  }

  protected List<Coords> getEmptyNeighbors(int x, int y, int range) {
    List<Coords> neighbors = new ArrayList<Coords>();
    for(int sx = x - range; sx <= x + range; sx++) {
      for(int sy = y - range; sy <= y + range; sy++) {
        if((sx != x || sy != y) && isInBounds(sx, sy) && grid.isEmpty(sx, sy)) {
          neighbors.add(tiles.get(sx, sy));
        }
      }
    }
    return neighbors;
  }

  public static List<Coords> getNeighbors(int x, int y, int range) {
    List<Coords> neighbors = new ArrayList<Coords>();
    for(int sx = x - range; sx <= x + range; sx++) {
      for(int sy = y - range; sy <= y + range; sy++) {
        if((sx != x || sy != y) && isInBounds(sx, sy)) {
          neighbors.add(tiles.get(sx, sy));
        }
      }
    }
    return neighbors;
  }

  public static boolean isInBounds(int x, int y) {
    return x >= -size / 2 && x <= size / 2 && y >= -size / 2 && y <= size / 2;
  }

  private class GridHelper implements EntitySubscription.SubscriptionListener {

    private Aspect.Builder aspect;
    private EntitySubscription subs;

    public GridHelper(Aspect.Builder aspect) {
      this.aspect = aspect;
    }

    public void initialize(World world) {
      subs = world.getAspectSubscriptionManager().get(aspect);
      subs.addSubscriptionListener(this);
    }

    public void processSystem() {
      IntBag actives = subs.getEntities();
      int[] ids = actives.getData();
      for(int i = 0; i < actives.size(); i++) {
        Coords latest = positions.get(ids[i]);
        Coords current = mGrid.get(ids[i]);
        if(latest == null) {
          grid.put(current.x(), current.y(), ids[i]);
          positions.put(ids[i], tiles.get(current.x(), current.y()));
          moved(ids[i]);
        } else if(current.x() != latest.x() || current.y() != latest.y()) {
          grid.remove(latest.x(), latest.y());
          grid.put(current.x(), current.y(), ids[i]);
          positions.put(ids[i], tiles.get(current.x(), current.y()));
          moved(ids[i]);
        }
      }
    }

    @Override
    public void inserted(IntBag entities) {
      int[] ids = entities.getData();
      for(int i = 0; i < entities.size(); i++) {
        Coords g = mGrid.get(ids[i]);
        grid.put(g.x(), g.y(), ids[i]);
        positions.put(ids[i], tiles.get(g.x(), g.y()));
        insertedIntoGrid(ids[i]);
      }
    }

    @Override
    public void removed(IntBag entities) {
      int[] ids = entities.getData();
      for(int i = 0; i < entities.size(); i++) {
        Coords g = positions.get(ids[i]);
        if(!grid.isEmpty(g.x(), g.y()) && grid.get(g.x(), g.y()) == ids[i]) {
          grid.remove(g.x(), g.y());
        }
        positions.remove(ids[i]);
        removedFromGrid(ids[i]);
      }
    }
  }
}
