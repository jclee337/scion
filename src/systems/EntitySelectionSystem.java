import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.utils.IntBag;
import org.joml.Matrix4f;
import org.joml.Vector2i;
import org.joml.Vector3f;
import org.joml.Vector4f;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.nuklear.Nuklear.*;
import static org.lwjgl.opengl.GL11.*;

public class EntitySelectionSystem extends BaseEntitySystem {

  private WindowSystem window;
  private RenderSystem render;
  private GUIRenderSystem gui;
  private Entity current;
  private Bindings bindings;
  private int indicator;
  private float hover;
  private ComponentMapper<Selectable> mSelect;
  private ComponentMapper<Selected> mSelected;
  private ComponentMapper<Transform> mTform;
  private ComponentMapper<Coords> mCoords;
  private Matrix4f invProj = new Matrix4f();
  private Matrix4f invView = new Matrix4f();

  public EntitySelectionSystem() {
    super(Aspect.all(Selectable.class, Transform.class));
  }

  @Override
  public void initialize() {
    initIndicator();
    Utils.listener(world, Aspect.all(Coords.class, Selected.class),
    (int inserted) -> {
      world.getMapper(Invisible.class).remove(indicator);
      Coords pos = mCoords.get(inserted);
      Transform t = mTform.create(indicator);
      Coords indPos = mCoords.create(indicator);
      indPos.move(pos.x(), pos.y());
      t.position.x = pos.x() * GridSystem.cell;
      t.position.y = pos.y() * GridSystem.cell;
    },
    (int removed) -> {
      world.getMapper(Invisible.class).create(indicator);
    });

    bindings = new Bindings(world);
    bindings.put(GLFW_MOUSE_BUTTON_LEFT, 0, GLFW_PRESS, () -> {
      IntBag actives = subscription.getEntities();
      int[] ids = actives.getData();

      float x = ((2.0f * bindings.mousex) / window.getWidth()) - 1.0f;
      float y = 1.0f - ((2.0f * bindings.mousey) / window.getHeight());

      Vector4f clip = new Vector4f(x, y, -1.0f, 1.0f);
      render.proj.invert(invProj);
      render.view.invert(invView);
      Vector4f eye = clip.mul(invProj);
      eye.z = -1.0f;
      eye.w = 0.0f;
      Vector4f model = eye.mul(invView).normalize();
      Vector3f ray = new Vector3f(model.x, model.y, model.z);
      Vector3f camera = mTform.get(render.camera).position;
      Entity selected = null;
      float min = Float.MAX_VALUE;
      for(int i = 0; i < actives.size(); i++) {
        Selectable select = mSelect.get(ids[i]);
        Transform pos = mTform.get(ids[i]);
        Vector3f dist = new Vector3f(0);
        camera.sub(pos.position, dist);
        float b = ray.dot(dist);
        float c = dist.dot(dist) - (select.radius * select.radius);
        float hit = (b * b) - c;

        /* hit */
        if(hit >= 0) {
          float t1 = -b + (float)Math.sqrt(hit);
          float t2 = -b - (float)Math.sqrt(hit);
          float t = Math.min(t1, t2);
          if(t < min) {
            selected = world.getEntity(ids[i]);
            min = t;
          }
        }
      }

      if(selected != null) {
        if(current != null) {
          mSelected.remove(current);
        }
        mSelected.create(selected);
        current = selected;
      } else if(current != null) {
        mSelected.remove(current);
        current = null;
      }
    });

    bindings.mode = Bindings.Mode.DEFERRING;
    bindings.enable();
  }

  @Override
  public void begin() {
    /*
    if(nk_window_is_any_hovered(gui.ctx)) {
      mouse = null;
    }
    */
  }

  @Override
  public void processSystem() {

  }

  private int initIndicator() {
    indicator = world.create();
    Mesh mesh = world.getMapper(Mesh.class).create(indicator);
    mesh.vertices = new float[] {
      -0.75f,  0.75f, 0.0f,
      0.75f, 0.75f, 0.0f,
      0.75f, -0.75f, 0.0f,
      -0.75f,  -0.75f, 0.0f,
      -0.75f,  0.75f, 0.3f,
      0.75f, 0.75f, 0.3f,
      0.75f, -0.75f, 0.3f,
      -0.75f,  -0.75f, 0.3f
    };
    mesh.colors = new float[] {
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      1.0f, 1.0f, 1.0f, 1.0f,
      0.0f, 0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 0.0f, 0.0f
    };
    mesh.indices = new int[] {
      0, 4, 5,
      0, 5, 1,
      1, 5, 2,
      2, 5, 6,
      2, 6, 7,
      2, 7, 3,
      3, 7, 0,
      0, 7, 4
    };
    mesh.mode = GL_TRIANGLES;
    return indicator;
  }
}
