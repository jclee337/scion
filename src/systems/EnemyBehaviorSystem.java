import com.artemis.Aspect;
import com.artemis.Component;
import com.artemis.ComponentMapper;
import com.artemis.EntitySubscription;
import com.artemis.systems.DelayedIteratingSystem;
import com.artemis.systems.IteratingSystem;
import com.artemis.utils.Bag;
import com.artemis.utils.IntBag;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.PriorityQueue;

public class EnemyBehaviorSystem extends DelayedIteratingSystem {

  private ComponentMapper<Controllable> mCont;
  private ComponentMapper<Coords> mGrid;
  private Grid<Integer> cost;
  private Grid<Integer> heuristic;
  private Grid<Coords> ancestor;
  private PriorityQueue<Coords> frontier;
  private EntitySubscription target;
  private EntitySubscription enemies;
  private int turn;
  private boolean attacked;
  private float delay;

  public EnemyBehaviorSystem() {
    super(Aspect.all(Enemy.class, Stats.class, Coords.class, Controllable.class));
  }

  @Override
  public void initialize() {
    super.initialize();
    Comparator<Coords> comp = (Coords g1, Coords g2) -> {
      return fValue(g1.x(), g1.y()) - fValue(g2.x(), g2.y());
    };
    frontier = new PriorityQueue(comp);

    Aspect.Builder aspect = Aspect.all(Ally.class, Stats.class, Coords.class);
    target = world.getAspectSubscriptionManager().get(aspect);

    aspect = Aspect.all(Enemy.class, ShardGrid.class, Coords.class);
    enemies = world.getAspectSubscriptionManager().get(aspect);

    /* Enemy spawn logic */
    Utils.listener(world, Aspect.all(ShardGrid.class, Controllable.class),
    (int inserted) -> {

    },
    (int removed) -> {
      IntBag actives = enemies.getEntities();
      if(actives.size() < 3 && Math.random() <= 0.2) {
        int one = Math.random() < 0.5 ? GridSystem.size / 2 + 1 : -GridSystem.size / 2 - 1;
        int two = (int)((Math.random() * GridSystem.size) - (GridSystem.size / 2));
        if(Math.random() <= 0.2) {
          Gen.genEnemy(world, one, two);
        } else {
          Gen.genEnemy(world, two, one);
        }
      }
    });

    /* Listen for damaged enemies */
    Utils.listener(world, Aspect.all(Attacked.class),
    (int inserted) -> {},
    removed -> {
      attacked = true;
      world.getMapper(Damaged.class).remove(removed);
      world.getMapper(Controllable.class).remove(turn);
    });
  }

  @Override
  public void inserted(int e) {
    turn = e;
    delay = 1000;
    super.inserted(e);
  }

  @Override
  public void processDelta(int e, float delta) {
    delay -= delta;
  }

  @Override
  public float getRemainingDelay(int e) {
    return delay;
  }

  @Override
  public void processExpired(int e) {
    Coords pos = mGrid.get(e);
    cost = new Grid<Integer>();
    cost.put(pos.x(), pos.y(), 0);
    heuristic = new Grid<Integer>();
    heuristic.put(pos.x(), pos.y(), 0);
    ancestor = new Grid<Coords>();
    frontier.clear();
    frontier.add(pos);
    // do A* path finding
    IntBag targets = target.getEntities();
    while(!frontier.isEmpty()) {
      Coords current = frontier.poll();

      if(!pos.grid().isEmpty(current.x(), current.y())) {
        Integer unit = pos.grid().get(current.x(), current.y());
        if(target.getAspect().isInterested(world.getEntity(unit))) {
          // found a path
          Coords nextStep = stepPath(pos, current);
          if(pos.grid().isEmpty(nextStep.x(), nextStep.y())) {
            pos.move(nextStep.x(), nextStep.y());
          }
          break;
        } else if(current != pos) {
          // found an obstacle
          //cost.remove(current.x, current.y);
          //heuristic.remove(current.x, current.y);
          //ancestor.remove(current.x, current.y);
          continue;
        }
      }
      for(Coords neighbor : GridSystem.getNeighbors(current.x(), current.y(), 1)) {
        int newCost = cost.get(current.x(), current.y()) + 1;
        if(cost.isEmpty(neighbor.x(), neighbor.y()) ||
        newCost < cost.get(neighbor.x(), neighbor.y())) {
          cost.put(neighbor.x(), neighbor.y(), newCost);
          heuristic.put(neighbor.x(), neighbor.y(), heuristic(neighbor.x(), neighbor.y(), targets));
          ancestor.put(neighbor.x(), neighbor.y(), current);
          frontier.add(neighbor);
        }
      }
    }

    ArrayList<Integer> allies = pos.grid().get(
    pos.x(), pos.y(), Grid.OMNI, new ArrayList<Integer>(),
    (x, y, t) -> world.getMapper(Ally.class).has(t));
    if(allies.isEmpty()) {
      world.getMapper(Controllable.class).remove(e);
    } else {
      Attacked a = world.getMapper(Attacked.class).create(allies.get((int)(Math.random() * allies.size())));
      a.attacker = world.getMapper(ShardGrid.class).get(e).total;
    }

    // finish
  }

  public Coords stepPath(Coords start, Coords end) {
    while(ancestor.get(end.x(), end.y()) != start) {
      end = ancestor.get(end.x(), end.y());
    }
    return end;
  }

  public int fValue(int x, int y) {
    return cost.get(x, y) + heuristic.get(x, y);
  }

  /**
  * Returns the average distance to all targets
  */
  public int heuristic(int x, int y, IntBag targets) {
    int[] ids = targets.getData();
    int total = 0;
    for(int i = 0; i < targets.size(); i++) {
      Coords t = mGrid.get(ids[i]);
      total += chebyshev(x, y, t.x(), t.y());
    }
    return total / targets.size();
  }

  public int chebyshev(int posx, int posy, int goalx, int goaly) {
    int dx = Math.abs(posx - goalx);
    int dy = Math.abs(posy - goaly);
    return (dx + dy) - Math.min(dx, dy);
    //return Math.max(dx, dy);
  }
}
