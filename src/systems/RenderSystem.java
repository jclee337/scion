
import com.artemis.*;
import com.artemis.World;
import com.artemis.systems.IntervalEntityProcessingSystem;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import org.lwjgl.BufferUtils;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

public class RenderSystem extends IntervalEntityProcessingSystem {

  private ComponentMapper<Transform> mTform;
  private ComponentMapper<Mesh> mMesh;
  private WindowSystem window;
  private ShaderProgram shaders;
  public Matrix4f model, view, proj;
  private float fov = (float)Math.toRadians(10.0);
  public int camera;

  public RenderSystem() {
    super(Aspect.all(Transform.class, Mesh.class).exclude(Invisible.class), 16.666666f);
    model = new Matrix4f();
    view = new Matrix4f();
    proj = new Matrix4f();
    try {
      shaders = new ShaderProgram();
      shaders.createVertexShader(Utils.loadResource("/shaders/default.vert"));
      shaders.createFragmentShader(Utils.loadResource("/shaders/default.frag"));
      shaders.link();
      shaders.createUniform("proj");
      shaders.createUniform("view");
      shaders.createUniform("model");
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  public void initialize() {
    initCamera();
  }

  @Override
  public void inserted(Entity e) {
    Mesh mesh = mMesh.get(e);

    mesh.vaoID = glGenVertexArrays();
    glBindVertexArray(mesh.vaoID);

    mesh.verticesVbo = glGenBuffers();
    FloatBuffer verticesBuffer = BufferUtils.createFloatBuffer(mesh.vertices.length);
    verticesBuffer.put(mesh.vertices).flip();
    glBindBuffer(GL_ARRAY_BUFFER, mesh.verticesVbo);
    glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_STREAM_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, false, 0, 0);

    mesh.colorsVbo = glGenBuffers();
    FloatBuffer colorBuffer = BufferUtils.createFloatBuffer(mesh.colors.length);
    colorBuffer.put(mesh.colors).flip();
    glBindBuffer(GL_ARRAY_BUFFER, mesh.colorsVbo);
    glBufferData(GL_ARRAY_BUFFER, colorBuffer, GL_STREAM_DRAW);
    glVertexAttribPointer(1, 4, GL_FLOAT, false, 0, 0);

    mesh.indicesVbo = glGenBuffers();
    IntBuffer indicesBuffer = BufferUtils.createIntBuffer(mesh.indices.length);
    indicesBuffer.put(mesh.indices).flip();
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.indicesVbo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer, GL_STREAM_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
  }

  @Override
  public void begin() {
    shaders.bind();
    Transform pos = mTform.get(camera);
    shaders.setUniform("proj", getProjectionMatrix(fov, 1280, 720, 0.1f, 1000.0f));
    shaders.setUniform("view", getViewMatrix(pos.position, new Vector3f(0, 0, 0)));
  }

  @Override
  public void process(Entity e) {
    Transform pos = mTform.get(e);
    Mesh mesh = mMesh.get(e);

    shaders.setUniform("model", getModelMatrix(pos.position, pos.rotation, pos.scale));

    glBindVertexArray(mesh.vaoID);
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    glDrawElements(mesh.mode, mesh.indices.length, GL_UNSIGNED_INT, 0);
    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glBindVertexArray(0);
    World world;
  }

  @Override
  public void end() {
    shaders.unbind();
    //glfwSwapBuffers(window.getWindow());
  }

  @Override
  public void removed(Entity e) {
    Mesh mesh = mMesh.get(e);

    glDisableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glDeleteBuffers(mesh.verticesVbo);
    glDeleteBuffers(mesh.colorsVbo);
    glDeleteBuffers(mesh.indicesVbo);

    glBindVertexArray(0);
    glDeleteVertexArrays(mesh.vaoID);
  }

  private void initCamera() {
    camera = world.create();
    Transform pos = mTform.create(camera);
    pos.position = new Vector3f(0.0f);
    pos.rotation = new Vector3f(0.0f);
    pos.scale = 1;
    world.getMapper(Camera.class).create(camera);
    world.getMapper(Controllable.class).create(camera);
  }

  public Matrix4f getProjectionMatrix(float fov, float width, float height, float zNear, float zFar) {
    float aspectRatio = width / height;
    proj.identity();
    proj.perspective(fov, aspectRatio, zNear, zFar);
    return proj;
  }

  public Matrix4f getViewMatrix(Vector3f position, Vector3f rotation) {
    view.identity();
    view.lookAt(position.x, position.y, position.z,
      0.0f, 0.0f, 0.0f,
      0.0f, 0.0f, 1.0f);
    // First do the rotation so camera rotates over its position
    //view.rotate((float)Math.toRadians(rotation.x), new Vector3f(1, 0, 0))
        //.rotate((float)Math.toRadians(rotation.y), new Vector3f(0, 1, 0));
    // Then do the translation
    //view.translate(-position.x, -position.y, -position.z);
    return view;
}

  public Matrix4f getModelMatrix(Vector3f offset, Vector3f rotation, float scale) {
    model.identity().translate(offset)
      .rotateX((float)Math.toRadians(rotation.x))
      .rotateY((float)Math.toRadians(rotation.y))
      .rotateZ((float)Math.toRadians(rotation.z))
      .scale(scale);
    return model;
  }
}
