
import com.artemis.BaseEntitySystem;
import com.artemis.systems.IteratingSystem;
import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.ComponentMapper;

public class StatsWindowSystem extends IteratingSystem {

  private WindowSystem window;
  private Integer selected;
  private UI ui;
  private boolean showGrid = false;
  private ComponentMapper<Stats> mStats;
  private ComponentMapper<ShardGrid> mShard;

  public StatsWindowSystem() {
    super(Aspect.all(ShardGrid.class, Selected.class));
  }

  @Override
  public void initialize() {
    ui = world.getMapper(UI.class).create(world.create());
    ui.setTitle("Stats")
    .move(0, window.getHeight() - 250)
    .resize(250, 250);
    ui.disable();
  }

  @Override
  public void inserted(int e) {
    ShardGrid shards = mShard.get(e);
    Stats stats = shards.total;
    ui.getRootWindow().clear();
    ui.getRootWindow().addWidget(
      new Row(20,
        new Text("Health: " + shards.health + "/" + stats.vitality, Text.LEFT)));
    ui.getRootWindow().addWidget(
      new Row(20,
        new Text("Vitality: " + stats.vitality, Text.LEFT)));
    ui.getRootWindow().addWidget(
      new Row(20,
        new Text("Precision: " + stats.precision, Text.LEFT)));
    ui.getRootWindow().addWidget(
      new Row(20,
        new Text("Strength: " + stats.strength, Text.LEFT)));
    ui.getRootWindow().addWidget(
      new Row(20,
        new Text("Speed: " + stats.speed, Text.LEFT)));
    ui.enable();
  }

  @Override
  public void process(int e) {

  }

  @Override
  public void removed(int e) {
    ui.disable();
  }
}
