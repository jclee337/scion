import com.artemis.Aspect;
import com.artemis.BaseEntitySystem;
import com.artemis.ComponentMapper;
import java.util.HashMap;

public class ArenaSystem extends BaseEntitySystem {
  private ComponentMapper<ShardGrid> mShards;
  private ComponentMapper<Coords> mCoords;
  private Grid<Integer> arena = new Grid<Integer>();
  private HashMap<Integer, Coords> coords = new HashMap<Integer, Coords>();

  public ArenaSystem() {
    super(Aspect.all(ShardGrid.class, Coords.class));
  }

  @Override
  public void initialize() {
    initSelectors();
  }

  @Override
  public void inserted(int e) {
    Coords c = mCoords.get(e);
    arena.put(c.x(), c.y(), e);
    coords.put(e, c);
    c.place(arena);
    c.onMoveDo((int entity) -> {
      Transition trans = world.getMapper(Transition.class).create(entity);
      Transform tform = world.getMapper(Transform.class).get(entity);
      trans.origin = new Transform(tform);
      trans.goal = new Transform(tform);
      trans.goal.position.x = c.x() * 1.5f;
      trans.goal.position.y = c.y() * 1.5f;
      trans.goal.scale = 1;
      trans.setBySpeed(0.007f);
    });
  }

  @Override
  public void removed(int e) {
    Coords c = coords.remove(e);
    arena.remove(c.x(), c.y());
  }

  @Override
  public void processSystem() {

  }

  public void initSelectors() {
    for(int x = -GridSystem.size / 2; x <= GridSystem.size / 2; x++) {
      for(int y = -GridSystem.size / 2; y <= GridSystem.size / 2; y++) {
        int e = world.create();
        Transform pos = world.getMapper(Transform.class).create(e);
        pos.position.x = GridSystem.cell * x;
        pos.position.y = GridSystem.cell * y;
        Selectable select = world.getMapper(Selectable.class).create(e);
        select.radius = 0.5f;
        Coords coords = mCoords.create(e);
        coords.move(x, y);
      }
    }
  }
}
