#version 330

in vec4 exColor;
out vec4 outColor;

void main() {
  outColor = exColor;
}
