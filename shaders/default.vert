#version 330

layout (location = 0) in vec3 pos;
layout (location = 1) in vec4 inColor;
out vec4 exColor;

uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

void main() {
  gl_Position = proj * view * model * vec4(pos, 1.0);
  exColor = inColor;
}
